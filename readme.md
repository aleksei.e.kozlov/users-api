[![pipeline status](https://gitlab.com/aleksei.e.kozlov/users-api/badges/main/pipeline.svg)](https://gitlab.com/aleksei.e.kozlov/users-api/-/commits/main)
[![coverage report](https://gitlab.com/aleksei.e.kozlov/users-api/badges/main/coverage.svg)](https://gitlab.com/aleksei.e.kozlov/users-api/-/commits/main)


# what this service is about
Handling user's lifecycle.

# requirements
* go 1.17
* docker, docker-compose
* make
* [optional] kafka

# how to interact with the service
* grpc api `0.0.0.0:90`
* swagger UI `http://0.0.0.0:80/swaggerui/`
* http rest gateway `http://0.0.0.0:80`
* healthcheck `http://0.0.0.0/live/`
* kafka topic with json notifications `localhost:9092` topic `users-api`

# usage example
* create a new user
  - Idempotency key: email
  - Kafka notification: yes
  - Validation: email is email, not empty password
  - id, created_at, updated_at are internal properties, generated automatically. Can bee seen in a response.
  - Returns created user
  - note that id field is generated by the service and it is uuid v4. But there is a hidden _id in mongodb that service ignores, except for stable sorting of search results. 
```
curl -X 'POST' \
  'http://0.0.0.0/v1/user' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "userMainValues": {
    "firstName": "Alice",
    "lastName": "Bob",
    "nickname": "carrot",
    "password": "123",
    "email": "alice+1@a.com",
    "country": "UK"
  }
}'
```
* modify a user
	- Error if nothing changed
	- Idempotency key: email
	- Kafka notification: yes
	- Validation: email is email, not empty password
	- bumps updated_at 
	- Returns old and new user states
	- If left some fields empty - it will make them empty in the database. So fill in all fields (can be obtained on creation or from listUsers call)
```
curl -X 'PATCH' \
  'http://0.0.0.0/v1/user/517fa5f7-a65a-444c-89c2-37d2d5a845a0' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "newMainValues": {
    "firstName": "Alice",
    "lastName": "Bob",
    "nickname": "potato",
    "password": "123",
    "email": "alice+3@a.com",
    "country": "UK"
  }
}'
```
* list users
  - Kafka notification: no
  - has configurable max results limit. If users page.size will exceed it - an error is returned. 
  - page.number starts with 0
  - Returns users array and NextPage, PreviousPage. If a page is nil - it means there is no next/previous page. 
  - has filter that allow searching by both user-defined and generated user fields. If many fields provided at once - search query will process them with logical AND. E.g. if given lastName=l1, id=id1 the request will be like "last_name = l1 AND id = id1". Empty fields are not included in a search request. 
```
curl -X 'GET' \
  'http://0.0.0.0/v1/users?page.number=0&page.size=2' \
  -H 'accept: application/json'
```
* remove user
	- idempotencty key: id
```
curl -X 'DELETE' \
  'http://0.0.0.0/v1/user/517fa5f7-a65a-444c-89c2-37d2d5a845a0' \
  -H 'accept: application/json'
```

# how config works
* it reads ENV variables, with fallback to a yaml file from `.configs` dir
* if no yaml file given - config reads only ENV
* `configs/values_local.yaml` is a default yaml file and only for bare host run without docker. It is out of git, so copy it from an example
```
cp .configs/values_local_example.yaml .configs/values_local.yaml
```
* docker environment has its own config
* special env var can pass a path to a yaml config `USERS_API_PATH_TO_CONFIG`

# how to build binary
* build binary in `.bin/` for host and `.bin_docker/` for docker
```
make build
# or 
docker compose up --build go-build
```

# how to run
* run 3rd party services
```
docker compose up --build -d mongodb kafka
```
* There are several environments to run code on: localhost, docker(local or gitlab CI)
  - bare local `go run`; prior to run, create a default config file (see `how config works`)
```
make go-run-local
```
  - execute a binary in docker. No need for a special config in docker environment; check Makefile to see how it passes the configPath
```
docker compose up --build run-binary
```
* it is easier to interact with the API from swagger UI
* service has a graceful shutdown on Ctr-C

# how to generate API
* check `.proto` file in `api` dir
* run grpc and proto linters (also detects breaking changes against master branch):
```
docker compose up --build proto_lint
```
* re-generate files
```
docker compose up --build compile_protobuf_files
```

# how to run golang linters
```
docker compose up --build go_lint
```

# local mongodb
* start
```
docker compose up --build -d mongodb
```
* to remove db completely delete dir `.mongodata`

# run tests
* prior to run create a default config file (see `how config works`)
* there are unit-tests, that require no external resources and integration-tests, that require mongodb. Integration tests have build tag `integration`
* see `internal/pkg/transport/grpcserver/grpc_server_integration_test.go` - this integration test runs entire grpc server and makes different grpc calls as golang client of a service. It uses dummy kafka producer, because kafka docker containers are too heavy to run on gitlab's CI pipelines. This is possible thanks to the DI container in `internal/pkg/other/di/container.go`
```
make test-unit
# or
docker compose up --build test-unit
```
* generate mocks
```
docker compose up --build generate_mocks
```
* integration tests
```
docker compose up --build -d mongodb
make test-integration
# or
docker compose up --build test-integration
```
* code coverage
```
docker compose up --build test-coverage
# check coverage.html
```

# check giltab CI
https://gitlab.com/aleksei.e.kozlov/users-api/-/pipelines

# state changes notifications with kafka
* run kafka in docker
```
docker compose up --build -d kafka
```
* That will create topic users-api (as described in docker-compose):
```
# 1 partition; 1 replicas
KAFKA_CREATE_TOPICS: "users-api:1:1"
```
* [optionally] consume from CLI to make sure that kafka and topic are available and to see produced messages:
```
# install (MacOS)
brew install kafka

# do in a separate terminal
kafka-console-consumer --bootstrap-server localhost:9092 --from-beginning --topic users-api
```
An example of a kafka message output(proto converted to json):
```json
{
	"add_user": {
		"user": {
			"user_main_values": {
				"first_name": "string",
				"last_name": "string",
				"nickname": "string",
				"password": "string",
				"email": "a2@a.com",
				"country": "string"
			},
			"user_generated_values": {
				"id": "8d5c4ead-4e1d-4d36-8b56-ad7a438948f8",
				"created_at": {
					"seconds": 1644225598
				},
				"updated_at": {
					"seconds": 1644225598
				}
			}
		}
	}
}
```
* readers have to decode json messages based on proto description

# suggested improvements
* do not store plain text password. Use hashing with salt.
* collect grpc prometheus metrics
* enhance password validation rules(and for other fields) by obtaining business requirements from a product manager. For now - it is only "not empty" check. See package `uservalidator` 
* add redis caching for read requests
* shard mongodb
* add helm chart that compiles values_producation.yaml and passes to k8s pod as env variables
* run multiple instances of a service
