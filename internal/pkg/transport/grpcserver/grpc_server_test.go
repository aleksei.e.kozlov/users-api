package grpcserver

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/transport/grpcserver/mocks"
	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
)

type testSuiteGrpcServer struct {
	suite.Suite

	logger *zap.Logger

	mockUserService *mocks.UserServiceProvider
	grpcServer      *GRPCServer
}

func (s *testSuiteGrpcServer) SetupTest() {
	s.logger = zaptest.NewLogger(s.T())

	s.mockUserService = &mocks.UserServiceProvider{}
	s.grpcServer = NewGRPCServer(s.logger, s.mockUserService)
}

func (s *testSuiteGrpcServer) TearDownTest() {
	s.mockUserService.AssertExpectations(s.T())
}

func TestSuiteGrpcServer(t *testing.T) {
	suite.Run(t, new(testSuiteGrpcServer))
}

func (s *testSuiteGrpcServer) Test_AddUser_CallService() {
	// arrange
	request := &users_apiv1.AddUserRequest{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: "asd",
		},
	}
	user := &users_apiv1.User{
		UserMainValues: request.UserMainValues,
		UserGeneratedValues: &users_apiv1.UserGeneratedValues{
			Id: "random_id",
		},
	}
	expected := &users_apiv1.AddUserResponse{
		User: user,
	}
	s.mockUserService.On("AddUser", mock.Anything, request.UserMainValues).Return(user, nil)

	// action
	actual, err := s.grpcServer.AddUser(context.Background(), request)

	// assert
	s.NoError(err)
	s.Equal(expected.String(), actual.String())
}

func (s *testSuiteGrpcServer) Test_AddUser_ProxyServiceError() {
	// arrange
	expectedErr := errors.New("your_err")
	request := &users_apiv1.AddUserRequest{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: "asd",
		},
	}
	s.mockUserService.On("AddUser", mock.Anything, request.UserMainValues).Return(nil, expectedErr)

	// action
	actual, err := s.grpcServer.AddUser(context.Background(), request)

	// assert
	s.Nil(actual)
	s.Error(err)
	s.Equal(expectedErr, err)
}

func (s *testSuiteGrpcServer) Test_ListUsers_CallService() {
	// arrange
	request := &users_apiv1.ListUsersRequest{
		Page: &users_apiv1.ListUserPage{
			Number: 1,
			Size:   2,
		},
		UserFilter: &users_apiv1.User{
			UserMainValues: &users_apiv1.UserMainValues{
				FirstName: "alice",
			},
			UserGeneratedValues: &users_apiv1.UserGeneratedValues{},
		},
	}
	expected := &users_apiv1.ListUsersResponse{
		Users: []*users_apiv1.User{
			&users_apiv1.User{
				UserMainValues: &users_apiv1.UserMainValues{FirstName: "alice"},
			},
		},
	}
	s.mockUserService.On("ListUsers", mock.Anything, request).Return(expected, nil)

	// action
	actual, err := s.grpcServer.ListUsers(context.Background(), request)

	// assert
	s.NoError(err)
	s.Equal(expected.String(), actual.String())
}

func (s *testSuiteGrpcServer) Test_ListUsers_ProxyServiceErr() {
	// arrange
	expectedErr := errors.New("err1")
	request := &users_apiv1.ListUsersRequest{
		UserFilter: &users_apiv1.User{
			UserMainValues: &users_apiv1.UserMainValues{
				FirstName: "alice",
			},
			UserGeneratedValues: &users_apiv1.UserGeneratedValues{},
		},
	}
	s.mockUserService.On("ListUsers", mock.Anything, request).Return(nil, expectedErr)

	// action
	actual, err := s.grpcServer.ListUsers(context.Background(), request)

	// assert
	s.Equal(expectedErr, err)
	s.Nil(actual)
}

func (s *testSuiteGrpcServer) Test_DeleteUser_ProxyToService() {
	// arrange
	request := &users_apiv1.DeleteUserRequest{
		Id: "asd",
	}
	expected := &users_apiv1.DeleteUserResponse{}
	s.mockUserService.On("DeleteUser", mock.Anything, request).Return(expected, nil)

	// action
	actual, err := s.grpcServer.DeleteUser(context.Background(), request)

	// assert
	s.NoError(err)
	s.Equal(expected, actual)
}

func (s *testSuiteGrpcServer) Test_ModifyUser_ProxyToService() {
	// arrange
	request := &users_apiv1.ModifyUserRequest{
		Id: "asd",
		NewMainValues: &users_apiv1.UserMainValues{
			FirstName: "eqwfwew",
		},
	}
	expected := &users_apiv1.ModifyUserResponse{}
	s.mockUserService.On("ModifyUser", mock.Anything, request).Return(expected, nil)

	// action
	actual, err := s.grpcServer.ModifyUser(context.Background(), request)

	// assert
	s.NoError(err)
	s.Equal(expected, actual)
}
