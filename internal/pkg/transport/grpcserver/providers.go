//go:generate mockery --case=underscore --name UserServiceProvider

package grpcserver

import (
	"context"

	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
)

type UserServiceProvider interface {
	AddUser(
		context.Context,
		*users_apiv1.UserMainValues,
	) (*users_apiv1.User, error)
	ListUsers(
		context.Context,
		*users_apiv1.ListUsersRequest,
	) (*users_apiv1.ListUsersResponse, error)
	DeleteUser(context.Context, *users_apiv1.DeleteUserRequest) (*users_apiv1.DeleteUserResponse, error)
	ModifyUser(context.Context, *users_apiv1.ModifyUserRequest) (*users_apiv1.ModifyUserResponse, error)
}
