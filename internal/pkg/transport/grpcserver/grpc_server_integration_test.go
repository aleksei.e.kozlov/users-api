//go:build integration
// +build integration

package grpcserver_test

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/other/config"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/other/di"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/transport/kafka"
	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/proto"
)

type testSuiteGrpcServerIntegration struct {
	suite.Suite

	logger *zap.Logger

	mongoClient *mongo.Client
	config      *config.Config
	grpcServer  *grpc.Server
}

func (s *testSuiteGrpcServerIntegration) SetupSuite() {
	s.logger = zaptest.NewLogger(s.T())

	pathToConfig := "../../../../.configs/values_local.yaml"
	if _, ok := os.LookupEnv("CI"); ok {
		pathToConfig = "../../../../.configs/values_docker.yaml"
	}
	ctxWithTimeout, cancelFn := context.WithTimeout(context.Background(), 4*time.Second)
	defer cancelFn()
	diContainer := di.NewContainer(
		ctxWithTimeout,
		pathToConfig,
	)
	diContainer.SetUsersChangesProducer(kafka.NewDummyProducer(diContainer.GetLogger())) // to avoid 1gb kafka docker container in ci
	s.config = diContainer.GetConfig()

	s.config.MongoUsersCollection += "_tests"
	var err error
	s.mongoClient, err = diContainer.GetMongoClient()
	s.NoError(err)

	// run grpc server
	grpcListener, err := diContainer.GetGRPCListener()
	if err != nil {
		s.logger.Fatal("failed to init grpcListener", zap.Error(err))
	}
	s.grpcServer, err = diContainer.GetGRPCServer()
	if err != nil {
		s.logger.Fatal("failed to init grpcServer", zap.Error(err))
	}
	go func() error {
		s.logger.Info("serving gRPC", zap.Any("grpcListener.Addr", grpcListener.Addr()))
		return s.grpcServer.Serve(grpcListener)
	}()
}

func (s *testSuiteGrpcServerIntegration) TearDownSuite() {
	s.grpcServer.GracefulStop()
}

func (s *testSuiteGrpcServerIntegration) SetupTest() {
	s.clearDB()
}

func (s *testSuiteGrpcServerIntegration) TearDownTest() {
	s.clearDB()
}

func (s *testSuiteGrpcServerIntegration) clearDB() {
	s.logger.Info("full clear on users collection")
	_, err := s.mongoClient.
		Database(s.config.MongoUsersDb).
		Collection(s.config.MongoUsersCollection).
		DeleteMany(context.Background(), bson.M{})
	s.NoError(err)
}

func TestSuiteGrpcServerIntegration(t *testing.T) {
	suite.Run(t, new(testSuiteGrpcServerIntegration))
}

func (s *testSuiteGrpcServerIntegration) Test_ListUsers_NoPanics() {
	// arrange
	conn, err := grpc.Dial(
		s.config.GrpcServerAddress,
		grpc.WithInsecure(),
	)
	s.NoError(err)
	defer conn.Close()

	client := users_apiv1.NewUsersApiServiceClient(conn)
	ctx := context.Background()

	// insert some data
	addResponse, err := client.AddUser(ctx, &users_apiv1.AddUserRequest{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: "alice",
			LastName:  "bob",
			Nickname:  "alicebob",
			Password:  "123",
			Email:     "a@a.com",
			Country:   "UK",
		},
	})
	s.NoError(err)

	type args struct {
		request *users_apiv1.ListUsersRequest
	}
	tests := []struct {
		name             string
		args             args
		expectedResponse *users_apiv1.ListUsersResponse
		allowErr         bool
	}{
		{
			name: "nil request.Page",
			args: args{
				request: &users_apiv1.ListUsersRequest{
					Page: nil,
				},
			},
			expectedResponse: nil,
			allowErr:         true,
		},
		{
			name: "nil request.UserFilter - return all",
			args: args{
				request: &users_apiv1.ListUsersRequest{
					Page: &users_apiv1.ListUserPage{
						Number: 0,
						Size:   2,
					},
					UserFilter: nil,
				},
			},
			expectedResponse: &users_apiv1.ListUsersResponse{
				Users: []*users_apiv1.User{
					addResponse.User,
				},
			},
			allowErr: false,
		},
		{
			name: "search by id",
			args: args{
				request: &users_apiv1.ListUsersRequest{
					Page: &users_apiv1.ListUserPage{
						Number: 0,
						Size:   2,
					},
					UserFilter: &users_apiv1.User{
						UserGeneratedValues: &users_apiv1.UserGeneratedValues{
							Id: addResponse.User.UserGeneratedValues.Id,
						},
					},
				},
			},
			expectedResponse: &users_apiv1.ListUsersResponse{
				Users: []*users_apiv1.User{
					addResponse.User,
				},
			},
			allowErr: false,
		},
		{
			name: "search by created_at",
			args: args{
				request: &users_apiv1.ListUsersRequest{
					Page: &users_apiv1.ListUserPage{
						Number: 0,
						Size:   2,
					},
					UserFilter: &users_apiv1.User{
						UserGeneratedValues: &users_apiv1.UserGeneratedValues{
							CreatedAt: addResponse.User.UserGeneratedValues.CreatedAt,
						},
					},
				},
			},
			expectedResponse: &users_apiv1.ListUsersResponse{
				Users: []*users_apiv1.User{
					addResponse.User,
				},
			},
			allowErr: false,
		},
		{
			name: "search by all fields at once",
			args: args{
				request: &users_apiv1.ListUsersRequest{
					Page: &users_apiv1.ListUserPage{
						Number: 0,
						Size:   2,
					},
					UserFilter: addResponse.User,
				},
			},
			expectedResponse: &users_apiv1.ListUsersResponse{
				Users: []*users_apiv1.User{
					addResponse.User,
				},
			},
			allowErr: false,
		},
	}
	for _, tt := range tests {
		s.Run(tt.name, func() {
			// action
			actualResponse, err := client.ListUsers(
				ctx,
				tt.args.request,
			)

			// assert
			s.Equal(
				tt.expectedResponse.String(),
				actualResponse.String(),
			)
			if tt.allowErr {
				s.Error(err)
			} else {
				s.NoError(err)
			}
		})
	}
}

func (s *testSuiteGrpcServerIntegration) Test_AddUsers_Idempotency() {
	// arrange
	conn, err := grpc.Dial(
		s.config.GrpcServerAddress,
		grpc.WithInsecure(),
	)
	s.NoError(err)
	defer conn.Close()

	client := users_apiv1.NewUsersApiServiceClient(conn)
	ctx := context.Background()

	// action
	addResponse1, err := client.AddUser(ctx, &users_apiv1.AddUserRequest{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: "alice",
			LastName:  "bob",
			Nickname:  "alicebob",
			Password:  "123",
			Email:     "a@a.com",
			Country:   "UK",
		},
	})
	s.NoError(err)
	addResponse2, err := client.AddUser(ctx, &users_apiv1.AddUserRequest{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: "alice",
			LastName:  "bob",
			Nickname:  "alicebob",
			Password:  "123",
			Email:     "a@a.com",
			Country:   "UK",
		},
	})

	// assert
	s.Error(err)
	s.Nil(addResponse2)
	s.Equal(addResponse1.User.UserMainValues.FirstName, "alice")
}

func (s *testSuiteGrpcServerIntegration) Test_ModifyUser_Success() {
	// arrange
	conn, err := grpc.Dial(
		s.config.GrpcServerAddress,
		grpc.WithInsecure(),
	)
	s.NoError(err)
	defer conn.Close()

	client := users_apiv1.NewUsersApiServiceClient(conn)
	ctx := context.Background()

	addResponse1, err := client.AddUser(ctx, &users_apiv1.AddUserRequest{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: "alice",
			LastName:  "bob",
			Nickname:  "alicebob",
			Password:  "123",
			Email:     "a@a.com",
			Country:   "UK",
		},
	})
	s.NoError(err)
	time.Sleep(2 * time.Second) // advance seconds to clearly see difference in updatedAt

	// action
	newMainValues := proto.Clone(addResponse1.User.UserMainValues).(*users_apiv1.UserMainValues)
	newMainValues.Nickname = "TOMAYO"
	modifyResponse, err := client.ModifyUser(ctx, &users_apiv1.ModifyUserRequest{
		Id:            addResponse1.User.UserGeneratedValues.Id,
		NewMainValues: newMainValues,
	})
	s.NoError(err)
	s.Equal(addResponse1.User.String(), modifyResponse.OldUser.String())
	s.NotEqual(addResponse1.User.String(), modifyResponse.ModifiedUser.String())
	s.Equal(newMainValues.Nickname, modifyResponse.ModifiedUser.UserMainValues.Nickname)
	s.Greater(
		modifyResponse.ModifiedUser.UserGeneratedValues.UpdatedAt.AsTime().Unix(),
		addResponse1.User.UserGeneratedValues.CreatedAt.AsTime().Unix(),
	)
	s.Greater(
		modifyResponse.ModifiedUser.UserGeneratedValues.UpdatedAt.AsTime().Unix(),
		modifyResponse.ModifiedUser.UserGeneratedValues.CreatedAt.AsTime().Unix(),
	)

	// assert
	listResponse, err := client.ListUsers(ctx, &users_apiv1.ListUsersRequest{
		Page: &users_apiv1.ListUserPage{
			Number: 0,
			Size:   1,
		},
		UserFilter: &users_apiv1.User{
			UserGeneratedValues: &users_apiv1.UserGeneratedValues{
				Id: addResponse1.User.UserGeneratedValues.Id,
			},
		},
	})
	s.NoError(err)
	s.Equal(1, len(listResponse.Users))
	s.Equal(modifyResponse.ModifiedUser.String(), listResponse.Users[0].String())
}

func (s *testSuiteGrpcServerIntegration) Test_ModifyUser_Idempotency() {
	// arrange
	conn, err := grpc.Dial(
		s.config.GrpcServerAddress,
		grpc.WithInsecure(),
	)
	s.NoError(err)
	defer conn.Close()

	client := users_apiv1.NewUsersApiServiceClient(conn)
	ctx := context.Background()

	addResponse1, err := client.AddUser(ctx, &users_apiv1.AddUserRequest{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: "alice",
			LastName:  "bob",
			Nickname:  "alicebob",
			Password:  "123",
			Email:     "a@a.com",
			Country:   "UK",
		},
	})
	s.NoError(err)
	time.Sleep(2 * time.Second) // advance seconds to clearly see difference in updatedAt

	// action
	newMainValues := proto.Clone(addResponse1.User.UserMainValues).(*users_apiv1.UserMainValues)
	newMainValues.Nickname = "TOMAYO"
	_, err = client.ModifyUser(ctx, &users_apiv1.ModifyUserRequest{
		Id:            addResponse1.User.UserGeneratedValues.Id,
		NewMainValues: newMainValues,
	})
	s.NoError(err)
	_, err = client.ModifyUser(ctx, &users_apiv1.ModifyUserRequest{
		Id:            addResponse1.User.UserGeneratedValues.Id,
		NewMainValues: newMainValues,
	})

	// assert
	s.Error(err)
}

func (s *testSuiteGrpcServerIntegration) Test_DeleteUser_Idempotency() {
	// arrange
	conn, err := grpc.Dial(
		s.config.GrpcServerAddress,
		grpc.WithInsecure(),
	)
	s.NoError(err)
	defer conn.Close()

	client := users_apiv1.NewUsersApiServiceClient(conn)
	ctx := context.Background()

	addResponse1, err := client.AddUser(ctx, &users_apiv1.AddUserRequest{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: "alice",
			LastName:  "bob",
			Nickname:  "alicebob",
			Password:  "123",
			Email:     "a@a.com",
			Country:   "UK",
		},
	})
	s.NoError(err)

	// action
	_, err = client.DeleteUser(ctx, &users_apiv1.DeleteUserRequest{
		Id: addResponse1.User.UserGeneratedValues.Id,
	})
	s.NoError(err)
	_, err = client.DeleteUser(ctx, &users_apiv1.DeleteUserRequest{
		Id: addResponse1.User.UserGeneratedValues.Id,
	})

	// assert
	s.Error(err)
}
