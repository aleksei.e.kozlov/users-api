package grpcserver

import (
	"context"

	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
	"go.uber.org/zap"
)

type GRPCServer struct {
	users_apiv1.UnimplementedUsersApiServiceServer

	logger      *zap.Logger
	userService UserServiceProvider
}

func NewGRPCServer(
	logger *zap.Logger,
	userService UserServiceProvider,
) *GRPCServer {
	return &GRPCServer{
		logger:      logger,
		userService: userService,
	}
}

func (s *GRPCServer) AddUser(ctx context.Context, request *users_apiv1.AddUserRequest) (*users_apiv1.AddUserResponse, error) {
	user, err := s.userService.AddUser(ctx, request.UserMainValues)
	if err != nil {
		s.logger.Error("can't AddUser", zap.Error(err))
		return nil, err
	}

	response := &users_apiv1.AddUserResponse{
		User: user,
	}
	return response, nil
}

func (s *GRPCServer) ListUsers(
	ctx context.Context,
	request *users_apiv1.ListUsersRequest,
) (*users_apiv1.ListUsersResponse, error) {
	return s.userService.ListUsers(ctx, request)
}

func (s *GRPCServer) DeleteUser(ctx context.Context, request *users_apiv1.DeleteUserRequest) (*users_apiv1.DeleteUserResponse, error) {
	return s.userService.DeleteUser(ctx, request)
}

func (s *GRPCServer) ModifyUser(ctx context.Context, request *users_apiv1.ModifyUserRequest) (*users_apiv1.ModifyUserResponse, error) {
	return s.userService.ModifyUser(ctx, request)
}
