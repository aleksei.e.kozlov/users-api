package kafka

import (
	"context"

	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
	"go.uber.org/zap"
)

type DummyProducer struct {
	logger *zap.Logger
}

func NewDummyProducer(logger *zap.Logger) *DummyProducer {
	return &DummyProducer{
		logger: logger,
	}
}

func (p *DummyProducer) NotifyAddUser(
	ctx context.Context,
	user *users_apiv1.User,
) error {
	p.logger.Info("NotifyAddUser", zap.Any("user", user.String()))
	return nil
}

func (p *DummyProducer) NotifyDeleteUser(
	ctx context.Context,
	id string,
) error {
	p.logger.Info("NotifyDeleteUser", zap.Any("id", id))
	return nil
}

func (p *DummyProducer) NotifyModifyUser(
	ctx context.Context,
	oldUser *users_apiv1.User,
	modifiedUser *users_apiv1.User,
) error {
	p.logger.Info(
		"NotifyModifyUser",
		zap.Any("oldUser", oldUser.String()),
		zap.Any("modifiedUser", modifiedUser.String()),
	)
	return nil
}

func (p *DummyProducer) Close() error {
	p.logger.Info("Close")
	return nil
}
