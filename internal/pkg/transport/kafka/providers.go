package kafka

import (
	"context"

	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
)

type KafkaProducerProvider interface {
	NotifyAddUser(
		ctx context.Context,
		user *users_apiv1.User,
	) error
	NotifyDeleteUser(
		ctx context.Context,
		id string,
	) error
	NotifyModifyUser(
		ctx context.Context,
		oldUser *users_apiv1.User,
		modifiedUser *users_apiv1.User,
	) error
	Close() error
}
