package kafka

import (
	"context"
	"encoding/json"

	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"

	"github.com/Shopify/sarama"
	"go.uber.org/zap"
)

type UsersChangesProducer struct {
	logger *zap.Logger

	producer sarama.AsyncProducer
	topic    string
}

func NewUsersChangesProducer(
	logger *zap.Logger,
	producer sarama.AsyncProducer,
	topic string,
) *UsersChangesProducer {
	return &UsersChangesProducer{
		logger:   logger,
		producer: producer,
		topic:    topic,
	}
}

func (p *UsersChangesProducer) Close() error {
	return p.producer.Close()
}

func (p *UsersChangesProducer) NotifyAddUser(
	ctx context.Context,
	user *users_apiv1.User,
) error {
	kafkaMessage := &users_apiv1.KafkaMessage{
		AddUser: &users_apiv1.KafkaMessage_KafkaAddUser{
			User: user,
		},
	}
	return p.produceMessage(kafkaMessage)
}

func (p *UsersChangesProducer) NotifyDeleteUser(
	ctx context.Context,
	id string,
) error {
	kafkaMessage := &users_apiv1.KafkaMessage{
		DeleteUser: &users_apiv1.KafkaMessage_KafkaDeleteUser{
			Id: id,
		},
	}
	return p.produceMessage(kafkaMessage)
}

func (p *UsersChangesProducer) NotifyModifyUser(
	ctx context.Context,
	oldUser *users_apiv1.User,
	modifiedUser *users_apiv1.User,
) error {
	kafkaMessage := &users_apiv1.KafkaMessage{
		ModifyUser: &users_apiv1.KafkaMessage_KafkaModifyUser{
			OldUser:      oldUser,
			ModifiedUser: modifiedUser,
		},
	}
	return p.produceMessage(kafkaMessage)
}

func (p *UsersChangesProducer) produceMessage(kafkaMessage *users_apiv1.KafkaMessage) error {
	kafkaMessageBytes, err := json.MarshalIndent(kafkaMessage, "", "\t")
	if err != nil {
		p.logger.Error("can't marshal proto to json", zap.Error(err))
		return err
	}

	message := &sarama.ProducerMessage{
		Topic: p.topic,
		Value: sarama.StringEncoder(kafkaMessageBytes),
	}
	p.producer.Input() <- message
	return nil
}
