package usersmongo

import (
	"context"
	"fmt"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"go.uber.org/zap"
)

type Storage struct {
	logger *zap.Logger

	usersDbName            string
	usersCollection        string
	mongoClient            *mongo.Client
	listUsersPageSizeLimit int64
}

func NewStorage(
	logger *zap.Logger,
	usersDbName string,
	usersCollection string,
	mongoClient *mongo.Client,
	listUsersPageSizeLimit int64,
) *Storage {
	return &Storage{
		logger:                 logger,
		usersDbName:            usersDbName,
		usersCollection:        usersCollection,
		mongoClient:            mongoClient,
		listUsersPageSizeLimit: listUsersPageSizeLimit,
	}
}

func (s *Storage) AddUser(ctx context.Context, user *model.User) error {
	user.TruncateToSecondsTimeFields()
	_, err := s.mongoClient.
		Database(s.usersDbName).
		Collection(s.usersCollection).
		InsertOne(ctx, user)
	if err != nil {
		s.logger.Error("can't insert in mongo", zap.Error(err), zap.Any("user", user))
		return err
	}
	return nil
}

func (s *Storage) ListUsers(
	ctx context.Context,
	page uint32,
	pageSize uint32,
	userFilter *model.User,
) (*model.ListUserResponse, error) {
	if pageSize > uint32(s.listUsersPageSizeLimit) {
		err := fmt.Errorf("pageSize is too big pageSize=%d, limit=%d", pageSize, s.listUsersPageSizeLimit)
		s.logger.Error("ListUsers", zap.Error(err))
		return nil, err
	}

	findOptions := options.Find()
	findOptions.SetSort(bson.D{primitive.E{Key: "_id", Value: 1}})
	findOptions.SetSkip(int64(page * pageSize))
	findOptions.SetLimit(int64(pageSize + 1)) // +1 to make sure that next page is exists

	findFilter := userFilter.GenerateBsonSearchFilter()

	cursor, err := s.mongoClient.
		Database(s.usersDbName).
		Collection(s.usersCollection).
		Find(ctx, findFilter, findOptions)
	if err != nil {
		s.logger.Error("ListUsers.find", zap.Error(err))
		return nil, err
	}
	defer func() {
		err = cursor.Close(ctx)
		if err != nil {
			s.logger.Error("can't close cursor, ignoring it", zap.Error(err))
		}
	}()

	foundUsers := []*model.User{}
	var previousPage *model.ListUserPage
	if page > 0 {
		previousPage = &model.ListUserPage{
			Page:     page - 1,
			PageSize: pageSize,
		}
	}
	var nextPage *model.ListUserPage
	i := 0
	for cursor.Next(ctx) {
		i++
		if i > int(pageSize) {
			nextPage = &model.ListUserPage{
				Page:     page + 1,
				PageSize: pageSize,
			}
			break
		}
		curUser := &model.User{}
		err := cursor.Decode(curUser)
		if err != nil {
			s.logger.Error("can't decode from cursor", zap.Error(err))
			return nil, err
		}
		foundUsers = append(foundUsers, curUser)
	}

	if len(foundUsers) == 0 {
		return &model.ListUserResponse{}, nil
	}
	return &model.ListUserResponse{
		Users:        foundUsers,
		NextPage:     nextPage,
		PreviousPage: previousPage,
	}, nil
}

func (s *Storage) DeleteUser(
	ctx context.Context,
	id uuid.UUID,
) error {
	result, err := s.mongoClient.
		Database(s.usersDbName).
		Collection(s.usersCollection).
		DeleteOne(ctx, bson.M{"id": id})
	if err != nil {
		s.logger.Error("can't DeleteOne", zap.Error(err))
		return err
	}
	if result.DeletedCount == 0 {
		err = fmt.Errorf("nothing to delete id=%s", id.String())
		s.logger.Error("DeleteUser got err", zap.Error(err))
		return err
	}
	return nil
}

func (s *Storage) GetUserByID(ctx context.Context, id uuid.UUID) (*model.User, error) {
	result := s.mongoClient.
		Database(s.usersDbName).
		Collection(s.usersCollection).
		FindOne(ctx, bson.M{"id": id})
	err := result.Err()
	if err != nil {
		s.logger.Error("GetUserByID got err", zap.Error(err))
		return nil, err
	}

	var user *model.User
	err = result.Decode(&user)
	if err != nil {
		s.logger.Error("GetUserByID can't decode response", zap.Error(err))
		return nil, err
	}
	return user, nil
}

func (s *Storage) ModifyUser(ctx context.Context, modifiedUser *model.User) error {
	modifiedUser.TruncateToSecondsTimeFields()
	opts := options.Update().SetUpsert(false)
	result, err := s.mongoClient.
		Database(s.usersDbName).
		Collection(s.usersCollection).
		UpdateOne(
			ctx,
			bson.M{"id": modifiedUser.ID},
			bson.D{
				{
					Key:   "$set",
					Value: modifiedUser.GenerateBsonUpdateFilter(),
				},
			},
			opts,
		)
	if err != nil {
		s.logger.Error("UpdateOne err", zap.Error(err))
		return err
	}
	if result.MatchedCount == 0 {
		err = fmt.Errorf("no matched documents id=%s", modifiedUser.ID.String())
		s.logger.Error("ModifyUser", zap.Error(err))
		return err
	}
	if result.UpsertedCount > 0 {
		err = fmt.Errorf("upsert happened for id=%s; that is horrible, please, contact your supervisor to fix the problem RIGHT NOW", modifiedUser.ID.String())
		s.logger.Error("ModifyUser", zap.Error(err))
		return err
	}
	return nil
}

func (s *Storage) Healthcheck() (string, error) {
	ctxTimeout, cancelFn := context.WithTimeout(context.Background(), 1*time.Second)
	defer cancelFn()
	if err := s.mongoClient.Ping(ctxTimeout, readpref.Primary()); err != nil {
		return fmt.Sprintf("users mongodb storage is NOT OK %s", err), err
	}
	return "users mongodb storage is ok", nil
}
