//go:build integration
// +build integration

package usersmongo_test

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/model"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/other/config"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/other/di"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/storage/usersmongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
)

type testSuiteUsersMongo struct {
	suite.Suite

	logger      *zap.Logger
	mongoClient *mongo.Client
	config      *config.Config

	storage *usersmongo.Storage
}

func (s *testSuiteUsersMongo) SetupSuite() {
	s.logger = zaptest.NewLogger(s.T())

	pathToConfig := "../../../../.configs/values_local.yaml"
	if _, ok := os.LookupEnv("CI"); ok {
		pathToConfig = "../../../../.configs/values_docker.yaml"
	}
	ctxWithTimeout, cancelFn := context.WithTimeout(context.Background(), 4*time.Second)
	defer cancelFn()
	diContainer := di.NewContainer(
		ctxWithTimeout,
		pathToConfig,
	)

	s.config = diContainer.GetConfig()
	s.config.MongoUsersCollection = s.config.MongoUsersCollection + "test"

	var err error
	s.mongoClient, err = diContainer.GetMongoClient()
	s.NoError(err)

	s.storage, err = diContainer.GetUsersMongoStorage()
	s.NoError(err)
}

func (s *testSuiteUsersMongo) SetupTest() {
	s.clearDB()
}

func (s *testSuiteUsersMongo) TearDownTest() {
	s.clearDB()
}

func (s *testSuiteUsersMongo) clearDB() {
	s.logger.Info("full clear on users collection")
	_, err := s.mongoClient.
		Database(s.config.MongoUsersDb).
		Collection(s.config.MongoUsersCollection).
		DeleteMany(context.Background(), bson.M{})
	s.NoError(err)
}

func (s *testSuiteUsersMongo) getAllUsersFromMongoSorted() []*model.User {
	ctx := context.Background()
	findOptions := options.Find()
	findOptions.SetSort(bson.D{primitive.E{Key: "_id", Value: -1}})
	cursor, err := s.mongoClient.
		Database(s.config.MongoUsersDb).
		Collection(s.config.MongoUsersCollection).
		Find(ctx, bson.D{}, findOptions)
	s.NoError(err)
	var actual []*model.User
	if err = cursor.All(ctx, &actual); err != nil {
		s.NoError(err)
	}
	return actual
}

func TestSuiteUsersMongo(t *testing.T) {
	suite.Run(t, new(testSuiteUsersMongo))
}

func (s *testSuiteUsersMongo) Test_AddUser_Success() {
	// arrange
	ctx := context.Background()
	now := time.Now().UTC()
	user := &model.User{
		ID:        uuid.NewV4(),
		FirstName: "alice",
		LastName:  "bob",
		Nickname:  "asdasd",
		Password:  "123",
		Email:     "a@asd.com",
		Country:   "a",
		CreatedAt: now,
		UpdatedAt: now,
	}
	expected := []*model.User{user}

	// action
	err := s.storage.AddUser(ctx, user)

	// assert
	s.NoError(err)

	actual := s.getAllUsersFromMongoSorted()
	s.Equal(expected, actual)
}

func (s *testSuiteUsersMongo) Test_AddUser_EmailIdempotency() {
	// arrange
	ctx := context.Background()
	now := time.Now().UTC()
	user1 := &model.User{
		ID:        uuid.NewV4(),
		FirstName: "alice",
		LastName:  "bob",
		Nickname:  "asdasd",
		Password:  "123",
		Email:     "a@asd.com",
		Country:   "a",
		CreatedAt: now,
		UpdatedAt: now,
	}
	user2 := &model.User{
		ID:        uuid.NewV4(),
		FirstName: "alice2",
		LastName:  "bob",
		Nickname:  "asdasd",
		Password:  "123",
		Email:     "a@asd.com", // !
		Country:   "a",
		CreatedAt: now,
		UpdatedAt: now,
	}
	expected := []*model.User{user1}

	// action
	err := s.storage.AddUser(ctx, user1)
	s.NoError(err)
	err = s.storage.AddUser(ctx, user2)

	// assert
	s.Error(err)

	actual := s.getAllUsersFromMongoSorted()
	s.Equal(expected, actual)
}

func (s *testSuiteUsersMongo) Test_AddUser_IDIdempotency() {
	// arrange
	ctx := context.Background()
	now := time.Now().UTC()
	uuid1 := uuid.NewV4()
	user1 := &model.User{
		ID:        uuid1,
		FirstName: "alice",
		LastName:  "bob",
		Nickname:  "asdasd",
		Password:  "123",
		Email:     "a@asd.com",
		Country:   "a",
		CreatedAt: now,
		UpdatedAt: now,
	}
	user2 := &model.User{
		ID:        uuid1, // !
		FirstName: "alice2",
		LastName:  "bob",
		Nickname:  "asdasd",
		Password:  "123",
		Email:     "a2@asd.com",
		Country:   "a",
		CreatedAt: now,
		UpdatedAt: now,
	}
	expected := []*model.User{user1}

	// action
	err := s.storage.AddUser(ctx, user1)
	s.NoError(err)
	err = s.storage.AddUser(ctx, user2)

	// assert
	s.Error(err)

	actual := s.getAllUsersFromMongoSorted()
	s.Equal(expected, actual)
}

func (s *testSuiteUsersMongo) Test_ListUsers_Add1Read1() {
	// arrange
	ctx := context.Background()
	now := time.Now().UTC()
	user1 := &model.User{
		ID:        uuid.NewV4(),
		FirstName: "alice",
		LastName:  "bob",
		Nickname:  "asdasd",
		Password:  "123",
		Email:     "a@asd.com",
		Country:   "a",
		CreatedAt: now,
		UpdatedAt: now,
	}
	err := s.storage.AddUser(ctx, user1)
	s.NoError(err)
	userFilter := &model.User{}

	expectedResponse := &model.ListUserResponse{
		Users:        []*model.User{user1},
		NextPage:     nil,
		PreviousPage: nil,
	}

	// action
	actualResponse, err := s.storage.ListUsers(ctx, 0, 2, userFilter)

	// assert
	s.NoError(err)
	s.Equal(expectedResponse, actualResponse)
}

func (s *testSuiteUsersMongo) insertUsers(n int) []*model.User {
	ctx := context.Background()
	now := time.Now().UTC()
	users := []*model.User{}
	for i := 0; i < n; i++ {
		user := &model.User{
			ID:        uuid.NewV4(), // unique
			FirstName: fmt.Sprintf("%dalice", i),
			LastName:  fmt.Sprintf("bob%d", i/2),
			Nickname:  "asdasd", // same for all
			Password:  fmt.Sprintf("123%d", i),
			Email:     fmt.Sprintf("%da@asd.com", i),
			Country:   fmt.Sprintf("country%d", i/3),
			CreatedAt: now.Add(time.Duration(i) * time.Second),
			UpdatedAt: now.Add(time.Duration(i) * time.Second),
		}
		err := s.storage.AddUser(ctx, user)
		s.NoError(err)
		users = append(users, user)
	}
	return users
}

func (s *testSuiteUsersMongo) Test_ListUsers_Add4ReadsWithOffsetsAndFilters() {
	// arrange
	ctx := context.Background()
	users := s.insertUsers(4)
	emptyUserFilter := &model.User{}

	// action
	// assert
	//		         0      1       2      3
	//		users=[first, second, third, fourth]
	tests := []struct {
		Name             string
		InputPage        model.ListUserPage
		InputFilter      *model.User
		ExpectedResponse *model.ListUserResponse
		ExpectedErr      bool
	}{
		{
			Name: "page=0;pageSize=2: read 2 users = first, second;emptyUserFilter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 2,
			},
			InputFilter: emptyUserFilter,
			ExpectedResponse: &model.ListUserResponse{
				Users: users[0:2],
				NextPage: &model.ListUserPage{
					Page:     1,
					PageSize: 2,
				},
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=1;pageSize=2: read 2 users = third, fourth;emptyUserFilter",
			InputPage: model.ListUserPage{
				Page:     1,
				PageSize: 2,
			},
			InputFilter: emptyUserFilter,
			ExpectedResponse: &model.ListUserResponse{
				Users:    users[2:4],
				NextPage: nil,
				PreviousPage: &model.ListUserPage{
					Page:     0,
					PageSize: 2,
				},
			},
			ExpectedErr: false,
		},
		{
			Name: "page=2;pageSize=2: return nothing;emptyUserFilter",
			InputPage: model.ListUserPage{
				Page:     2,
				PageSize: 2,
			},
			InputFilter:      emptyUserFilter,
			ExpectedResponse: &model.ListUserResponse{},
			ExpectedErr:      false,
		},
		{
			Name: "page=0;pageSize=3: read 3 users = first, second, third;emptyUserFilter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 3,
			},
			InputFilter: emptyUserFilter,
			ExpectedResponse: &model.ListUserResponse{
				Users: users[0:3],
				NextPage: &model.ListUserPage{
					Page:     1,
					PageSize: 3,
				},
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=1;pageSize=3: read 1 user = fourth;emptyUserFilter",
			InputPage: model.ListUserPage{
				Page:     1,
				PageSize: 3,
			},
			InputFilter: emptyUserFilter,
			ExpectedResponse: &model.ListUserResponse{
				Users:    users[3:4],
				NextPage: nil,
				PreviousPage: &model.ListUserPage{
					Page:     0,
					PageSize: 3,
				},
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=33: read 4 users = first, second, third, fourth;emptyUserFilter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 33,
			},
			InputFilter: emptyUserFilter,
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[0:4],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=4: read 4 users = first, second, third, fourth;emptyUserFilter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 4,
			},
			InputFilter: emptyUserFilter,
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[0:4],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=2: read 1 user = first;id filter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 2,
			},
			InputFilter: &model.User{
				ID: users[0].ID,
			},
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[0:1],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=2: read 1 user = first;first_name filter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 2,
			},
			InputFilter: &model.User{
				FirstName: users[0].FirstName,
			},
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[0:1],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=4: read 2 users = first, second;last_name filter; i mod 2",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 4,
			},
			InputFilter: &model.User{
				LastName: users[0].LastName,
			},
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[0:2],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=4: read 4 users = first, second, third, fourth;nickname filter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 4,
			},
			InputFilter: &model.User{
				Nickname: users[0].Nickname,
			},
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[0:4],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=4: read 1 user = first;password filter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 4,
			},
			InputFilter: &model.User{
				Password: users[0].Password,
			},
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[0:1],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=4: read 1 user = fourth;email filter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 4,
			},
			InputFilter: &model.User{
				Email: users[3].Email,
			},
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[3:4],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=4: read 1 user = fourth;country filter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 4,
			},
			InputFilter: &model.User{
				Country: users[3].Country,
			},
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[3:4],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=4: read 1 user = fourth;created_at filter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 4,
			},
			InputFilter: &model.User{
				CreatedAt: users[3].CreatedAt,
			},
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[3:4],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=4: read 1 user = fourth;updated_at filter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 4,
			},
			InputFilter: &model.User{
				UpdatedAt: users[3].UpdatedAt,
			},
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[3:4],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=4: read 1 user = second;country and first_name filter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 4,
			},
			InputFilter: &model.User{
				Country:   users[1].Country,
				FirstName: users[1].FirstName,
			},
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[1:2],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=4: read 1 user = first, second;country and last_name filter",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 4,
			},
			InputFilter: &model.User{
				Country:  users[1].Country,
				LastName: users[1].LastName,
			},
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[0:2],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
		{
			Name: "page=0;pageSize=4: read 1 user = first, second;filter all fields",
			InputPage: model.ListUserPage{
				Page:     0,
				PageSize: 4,
			},
			InputFilter: users[1],
			ExpectedResponse: &model.ListUserResponse{
				Users:        users[1:2],
				NextPage:     nil,
				PreviousPage: nil,
			},
			ExpectedErr: false,
		},
	}
	for _, test := range tests {
		s.Run(test.Name, func() {
			// action
			actualResponse, err := s.storage.ListUsers(
				ctx,
				test.InputPage.Page,
				test.InputPage.PageSize,
				test.InputFilter,
			)
			s.Equal(test.ExpectedResponse, actualResponse)
			if test.ExpectedErr {
				s.Error(err)
			} else {
				s.NoError(err)
			}
		})
	}
}

func (s *testSuiteUsersMongo) Test_ListUsers_FailOnPageSizeLimit() {
	// arrange
	ctx := context.Background()
	userFilter := &model.User{}

	// action
	actualUsers, err := s.storage.ListUsers(ctx, 0, 9999999, userFilter)

	// assert
	s.Error(err)
	s.Nil(actualUsers)
}

func (s *testSuiteUsersMongo) Test_DeleteUser_Success() {
	// arrange
	ctx := context.Background()
	user := &model.User{
		ID: uuid.NewV4(),
	}
	s.storage.AddUser(ctx, user)

	insertedUser, err := s.storage.GetUserByID(ctx, user.ID)
	s.NoError(err)
	s.Equal(insertedUser, user)

	// action
	err = s.storage.DeleteUser(ctx, user.ID)

	// assert
	s.NoError(err)

	insertedUser, err = s.storage.GetUserByID(ctx, user.ID)
	s.Error(err)
	s.Nil(insertedUser)
}

func (s *testSuiteUsersMongo) Test_DeleteUser_Unexisted() {
	// arrange
	ctx := context.Background()
	user := &model.User{
		ID: uuid.NewV4(),
	}

	// action
	err := s.storage.DeleteUser(ctx, user.ID)

	// assert
	s.Error(err)

	insertedUser, err := s.storage.GetUserByID(ctx, user.ID)
	s.Error(err)
	s.Nil(insertedUser)
}

func (s *testSuiteUsersMongo) Test_ModifyUser_Success() {
	// arrange
	ctx := context.Background()
	t1 := time.Now().UTC()
	t2 := t1.Add(time.Second)
	originalUser := &model.User{
		ID:        uuid.NewV4(),
		FirstName: "myFirstName",
		LastName:  "myLastName",
		Nickname:  "myNickname",
		Password:  "myPassword",
		Email:     "myEmail",
		Country:   "myCountry",
		CreatedAt: t1,
		UpdatedAt: t1,
	}
	s.NoError(s.storage.AddUser(ctx, originalUser))

	insertedUser, err := s.storage.GetUserByID(ctx, originalUser.ID)
	s.NoError(err)
	s.Equal(insertedUser, originalUser)

	inputModifyUser := &model.User{
		ID:        originalUser.ID, // no changes
		FirstName: originalUser.FirstName + "1",
		LastName:  originalUser.LastName + "1",
		Nickname:  originalUser.Nickname + "1",
		Password:  originalUser.Password + "1",
		Email:     originalUser.Email + "1",
		Country:   originalUser.Country + "1",
		CreatedAt: time.Time{}, // empty
		UpdatedAt: t2,
	}
	expectedUser := &model.User{
		ID:        originalUser.ID, // no changes
		FirstName: originalUser.FirstName + "1",
		LastName:  originalUser.LastName + "1",
		Nickname:  originalUser.Nickname + "1",
		Password:  originalUser.Password + "1",
		Email:     originalUser.Email + "1",
		Country:   originalUser.Country + "1",
		CreatedAt: originalUser.CreatedAt, // no changes
		UpdatedAt: t2,
	}
	expectedUser.TruncateToSecondsTimeFields()

	// action
	err = s.storage.ModifyUser(ctx, inputModifyUser)

	// assert
	s.NoError(err)

	actualUser, err := s.storage.GetUserByID(ctx, originalUser.ID)
	s.NoError(err)
	s.Equal(expectedUser, actualUser)
}

func (s *testSuiteUsersMongo) Test_ModifyUser_FailUnexistedID() {
	// arrange
	ctx := context.Background()
	t1 := time.Now().UTC()
	t2 := t1.Add(time.Second)
	originalUser := &model.User{
		ID:        uuid.NewV4(),
		FirstName: "myFirstName",
		LastName:  "myLastName",
		Nickname:  "myNickname",
		Password:  "myPassword",
		Email:     "myEmail",
		Country:   "myCountry",
		CreatedAt: t1,
		UpdatedAt: t1,
	}
	// s.NoError(s.storage.AddUser(ctx, originalUser))

	// insertedUser, err := s.storage.GetUserByID(ctx, originalUser.ID)
	// s.NoError(err)
	// s.Equal(insertedUser, originalUser)

	inputModifyUser := &model.User{
		ID:        originalUser.ID, // no changes
		FirstName: originalUser.FirstName + "1",
		LastName:  originalUser.LastName + "1",
		Nickname:  originalUser.Nickname + "1",
		Password:  originalUser.Password + "1",
		Email:     originalUser.Email + "1",
		Country:   originalUser.Country + "1",
		CreatedAt: time.Time{}, // empty
		UpdatedAt: t2,
	}

	// action
	err := s.storage.ModifyUser(ctx, inputModifyUser)

	// assert
	s.Error(err)

	actualUser, err := s.storage.GetUserByID(ctx, originalUser.ID)
	s.Error(err)
	s.Nil(actualUser)
}
