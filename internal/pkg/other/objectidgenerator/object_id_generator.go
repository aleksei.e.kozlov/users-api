package objectidgenerator

import uuid "github.com/satori/go.uuid"

type ObjectIDGenerator struct{}

func NewObjectIDGenerator() *ObjectIDGenerator {
	return &ObjectIDGenerator{}
}

func (g *ObjectIDGenerator) NewObjectId() uuid.UUID {
	return uuid.NewV4()
}
