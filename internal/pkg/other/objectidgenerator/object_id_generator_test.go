package objectidgenerator

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/suite"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
)

type testSuiteObjectIDGenerator struct {
	suite.Suite

	logger *zap.Logger

	generator *ObjectIDGenerator
}

func (s *testSuiteObjectIDGenerator) SetupTest() {
	s.logger = zaptest.NewLogger(s.T())

	s.generator = NewObjectIDGenerator()
}

func (s *testSuiteObjectIDGenerator) TearDownTest() {
	// empty yet
}

func TestSuiteObjectIDGenerator(t *testing.T) {
	suite.Run(t, new(testSuiteObjectIDGenerator))
}

func (s *testSuiteObjectIDGenerator) Test_NewObjectId_NotEmpty() {
	// arrange

	// action
	actual := s.generator.NewObjectId()
	s.logger.Info("", zap.Any("actual", actual))

	// assert
	s.NotEqual(uuid.Nil, actual)
}

func (s *testSuiteObjectIDGenerator) Test_NewObjectId_EveryTimeNew() {
	// arrange

	// action
	first := s.generator.NewObjectId()
	second := s.generator.NewObjectId()

	// assert
	s.NotEqual(first, second)
}
