package healthcheck

import "fmt"

type HealthCheckService struct {
	services []ServiceProvider
}

func NewHealthCheckService(services ...ServiceProvider) *HealthCheckService {
	return &HealthCheckService{
		services: services,
	}
}

func (s *HealthCheckService) Healthcheck() (string, error) {
	description := ""
	var firstErr error
	for _, service := range s.services {
		serviceDescription, serviceErr := service.Healthcheck()
		if firstErr == nil {
			firstErr = serviceErr
		}
		description += fmt.Sprintf("%s\n", serviceDescription)
	}
	return description, firstErr
}
