package healthcheck

type ServiceProvider interface {
	Healthcheck() (string, error)
}
