package clockutc

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
)

type testSuiteClockUTC struct {
	suite.Suite

	logger *zap.Logger

	clockUTC *ClockUTC
}

func (s *testSuiteClockUTC) SetupTest() {
	s.logger = zaptest.NewLogger(s.T())

	s.clockUTC = NewClockUTC()
}

func (s *testSuiteClockUTC) TearDownTest() {
	// empty yet
}

func TestSuiteClockUTC(t *testing.T) {
	suite.Run(t, new(testSuiteClockUTC))
}

func (s *testSuiteClockUTC) Test_Now() {
	// arrange

	// action
	actual := s.clockUTC.NowUTC()
	s.logger.Info("now utc", zap.Any("actual", actual))

	// assert
	s.NotEqual(time.Time{}, actual)
}
