package clockutc

import "time"

type ClockUTC struct{}

func NewClockUTC() *ClockUTC {
	return &ClockUTC{}
}

func (c *ClockUTC) NowUTC() time.Time {
	return time.Now().UTC()
}
