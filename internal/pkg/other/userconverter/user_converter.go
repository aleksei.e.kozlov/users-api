package userconverter

import (
	"errors"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/model"
	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type UserConverter struct{}

func NewUserConverter() *UserConverter {
	return &UserConverter{}
}

func (c *UserConverter) ModelToGRPC(modelUser *model.User) *users_apiv1.User {
	grpcUser := &users_apiv1.User{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: modelUser.FirstName,
			LastName:  modelUser.LastName,
			Nickname:  modelUser.Nickname,
			Password:  modelUser.Password,
			Email:     modelUser.Email,
			Country:   modelUser.Country,
		},
		UserGeneratedValues: &users_apiv1.UserGeneratedValues{
			Id:        modelUser.ID.String(),
			CreatedAt: timestamppb.New(modelUser.CreatedAt),
			UpdatedAt: timestamppb.New(modelUser.UpdatedAt),
		},
	}
	return grpcUser
}

func (c *UserConverter) GRPCToModel(grpcUser *users_apiv1.User) (*model.User, error) {
	if grpcUser == nil {
		return nil, errors.New("nil grpcUser")
	}
	if grpcUser.UserMainValues == nil {
		return nil, errors.New("nil grpcUser.UserMainValues")
	}
	if grpcUser.UserGeneratedValues == nil {
		return nil, errors.New("nil grpcUser.UserGeneratedValues")
	}

	modelUser := &model.User{
		ID:        uuid.FromStringOrNil(grpcUser.UserGeneratedValues.Id),
		FirstName: grpcUser.UserMainValues.FirstName,
		LastName:  grpcUser.UserMainValues.LastName,
		Nickname:  grpcUser.UserMainValues.Nickname,
		Password:  grpcUser.UserMainValues.Password,
		Email:     grpcUser.UserMainValues.Email,
		Country:   grpcUser.UserMainValues.Country,
		CreatedAt: grpcUser.UserGeneratedValues.CreatedAt.AsTime(),
		UpdatedAt: grpcUser.UserGeneratedValues.UpdatedAt.AsTime(),
	}

	// proto timestamp has default at the start of unix epoch (1970), but golang time has default at the first year (1)
	if grpcUser.UserGeneratedValues.CreatedAt == nil {
		modelUser.CreatedAt = time.Time{}
	}
	if grpcUser.UserGeneratedValues.UpdatedAt == nil {
		modelUser.UpdatedAt = time.Time{}
	}

	return modelUser, nil
}
