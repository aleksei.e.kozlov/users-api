package userconverter

import (
	"testing"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/model"
	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type testSuiteUserConverter struct {
	suite.Suite

	logger *zap.Logger

	converter *UserConverter
}

func (s *testSuiteUserConverter) SetupTest() {
	s.logger = zaptest.NewLogger(s.T())

	s.converter = NewUserConverter()
}

func (s *testSuiteUserConverter) TearDownTest() {
	// empty yet
}

func TestSuiteUserConverter(t *testing.T) {
	suite.Run(t, new(testSuiteUserConverter))
}

func (s *testSuiteUserConverter) Test_ModelToGRPC() {
	// arrange
	createdAt := time.Now().UTC()
	updatedAt := createdAt.Add(time.Second)
	modelUser := &model.User{
		ID:        uuid.FromStringOrNil("123e4567-e89b-12d3-a456-426614174000"),
		FirstName: "alice",
		LastName:  "bob",
		Nickname:  "alicebob",
		Password:  "123",
		Email:     "a@b.com",
		Country:   "aliceland",
		CreatedAt: createdAt,
		UpdatedAt: updatedAt,
	}
	expectedGrpcUser := &users_apiv1.User{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: "alice",
			LastName:  "bob",
			Nickname:  "alicebob",
			Password:  "123",
			Email:     "a@b.com",
			Country:   "aliceland",
		},
		UserGeneratedValues: &users_apiv1.UserGeneratedValues{
			Id:        "123e4567-e89b-12d3-a456-426614174000",
			CreatedAt: timestamppb.New(createdAt),
			UpdatedAt: timestamppb.New(updatedAt),
		},
	}

	// action
	actualGrpcUser := s.converter.ModelToGRPC(modelUser)

	// assert
	s.Equal(expectedGrpcUser, actualGrpcUser)
}

func (s *testSuiteUserConverter) Test_ModelToGRPC_EmptyDefault() {
	// arrange
	modelUser := &model.User{}
	expectedGrpcUser := &users_apiv1.User{
		UserMainValues: &users_apiv1.UserMainValues{},
		UserGeneratedValues: &users_apiv1.UserGeneratedValues{
			Id:        "00000000-0000-0000-0000-000000000000",
			CreatedAt: timestamppb.New(time.Time{}),
			UpdatedAt: timestamppb.New(time.Time{}),
		},
	}

	// action
	actualGrpcUser := s.converter.ModelToGRPC(modelUser)

	// assert
	s.Equal(expectedGrpcUser, actualGrpcUser)
}

func (s *testSuiteUserConverter) Test_GRPCToModel() {
	// arrange
	createdAt := time.Date(2022, 02, 01, 02, 02, 01, 123, time.UTC)
	updatedAt := createdAt.Add(time.Second)
	expectedModelUser := &model.User{
		ID:        uuid.FromStringOrNil("123e4567-e89b-12d3-a456-426614174000"),
		FirstName: "alice",
		LastName:  "bob",
		Nickname:  "alicebob",
		Password:  "123",
		Email:     "a@b.com",
		Country:   "aliceland",
		CreatedAt: createdAt,
		UpdatedAt: updatedAt,
	}
	grpcUser := &users_apiv1.User{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: "alice",
			LastName:  "bob",
			Nickname:  "alicebob",
			Password:  "123",
			Email:     "a@b.com",
			Country:   "aliceland",
		},
		UserGeneratedValues: &users_apiv1.UserGeneratedValues{
			Id:        "123e4567-e89b-12d3-a456-426614174000",
			CreatedAt: timestamppb.New(createdAt),
			UpdatedAt: timestamppb.New(updatedAt),
		},
	}

	// action
	actualGrpcUser, err := s.converter.GRPCToModel(grpcUser)

	// assert
	s.NoError(err)
	s.Equal(expectedModelUser, actualGrpcUser)
}

func (s *testSuiteUserConverter) Test_GRPCToModel_DefaultValues() {
	// arrange
	grpcUser := &users_apiv1.User{
		UserMainValues:      &users_apiv1.UserMainValues{},
		UserGeneratedValues: &users_apiv1.UserGeneratedValues{},
	}
	expectedModelUser := &model.User{}

	// action
	actualGrpcUser, err := s.converter.GRPCToModel(grpcUser)

	// assert
	s.NoError(err)
	s.Equal(expectedModelUser, actualGrpcUser)
}

func (s *testSuiteUserConverter) Test_GRPCToModel_InalidUUID() {
	// arrange
	expectedModelUser := &model.User{}
	grpcUser := &users_apiv1.User{
		UserMainValues: &users_apiv1.UserMainValues{},
		UserGeneratedValues: &users_apiv1.UserGeneratedValues{
			Id: "IAMINVALID",
		},
	}

	// action
	actualGrpcUser, err := s.converter.GRPCToModel(grpcUser)

	// assert
	s.NoError(err)
	s.Equal(expectedModelUser, actualGrpcUser)
}
