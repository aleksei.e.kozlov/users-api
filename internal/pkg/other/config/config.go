package config

import (
	"io/ioutil"
	"os"
	"strconv"

	"go.uber.org/zap"
	"gopkg.in/yaml.v3"
)

const (
	envPathToConfigKey  = "USERS_API_PATH_TO_CONFIG" // we need it to specify config yaml path to run cmd
	defaultPathToConfig = ".configs/values_local.yaml"
)

type Config struct {
	logger *zap.Logger

	GrpcServerAddress        string
	GrpcGatewayServerAddress string
	SwaggerUiDir             string
	SwaggerHttpPath          string
	MongoURI                 string
	MongoUsersDb             string
	MongoUsersCollection     string
	ListUsersPageSizeLimit   int64
	HealthcheckHttpPath      string
	KafkaProduceTopic        string
	KafkaBroker              string
}

func NewConfig(logger *zap.Logger, pathToConfig string) *Config {
	logger.Info(
		"config will read values from ENV with a fallback to yaml",
		zap.Any("pathToConfig", pathToConfig),
	)

	yamlFileContent, err := readYamlFileContent(logger, pathToConfig)
	if err != nil {
		logger.Error(
			"can't read config from yaml. Will continue only with ENV variables",
			zap.Any("pathToConfig", pathToConfig),
			zap.Error(err),
		)
	}
	return &Config{
		logger: logger,

		GrpcServerAddress:        getEnvWithFallback("USERS_API_GRPC_SERVER_ADDRESS", yamlFileContent),
		GrpcGatewayServerAddress: getEnvWithFallback("USERS_API_GRPC_GATEWAY_SERVER_ADDRESS", yamlFileContent),
		SwaggerUiDir:             getEnvWithFallback("USERS_API_SWAGGER_UI_DIR", yamlFileContent),
		SwaggerHttpPath:          getEnvWithFallback("USERS_API_SWAGGER_HTTP_PATH", yamlFileContent),
		MongoURI:                 getEnvWithFallback("USERS_API_MONGO_URI", yamlFileContent),
		MongoUsersDb:             getEnvWithFallback("USERS_API_MONGO_USERS_DB", yamlFileContent),
		MongoUsersCollection:     getEnvWithFallback("USERS_API_MONGO_USERS_COLLECTION", yamlFileContent),
		ListUsersPageSizeLimit: toInt64OrDefault(
			getEnvWithFallback("USERS_API_LIST_USERS_PAGE_SIZE_LIMIT", yamlFileContent),
			1000,
		),
		HealthcheckHttpPath: getEnvWithFallback("USERS_API_HEALTHCHECK_HTTP_PATH", yamlFileContent),
		KafkaProduceTopic:   getEnvWithFallback("USERS_API_KAFKA_PRODUCE_TOPIC", yamlFileContent),
		KafkaBroker:         getEnvWithFallback("USERS_API_KAFKA_BROKER", yamlFileContent),
	}
}

func toInt64OrDefault(s string, defaultValue int64) int64 {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return defaultValue
	}
	if i == 0 {
		return defaultValue
	}
	return i
}

func getEnvWithFallback(key string, fallback *yamlFile) string {
	envValue, exists := os.LookupEnv(key)
	if !exists {
		if fallback != nil {
			return fallback.EnvDict[key]
		}
		return ""
	}
	return envValue
}

// DTO to parse yaml files
type yamlFile struct {
	EnvDict map[string]string `yaml:"env,omitempty"`
}

func readYamlFileContent(logger *zap.Logger, pathToConfig string) (*yamlFile, error) {
	yamlFileContent := &yamlFile{}

	if len(pathToConfig) == 0 {
		if envPathToConfigValue := os.Getenv(envPathToConfigKey); len(envPathToConfigValue) > 0 {
			pathToConfig = envPathToConfigValue
		} else {
			if _, err := os.Stat(defaultPathToConfig); err == nil {
				logger.Info("will try default config path", zap.Any("defaultPathToConfig", defaultPathToConfig))
				pathToConfig = defaultPathToConfig
			} else {
				return nil, nil
			}
		}
	}

	yamlFile, err := ioutil.ReadFile(pathToConfig)
	if err != nil {
		logger.Error(
			"can't read file",
			zap.Any("pathToConfig", pathToConfig),
			zap.Error(err),
		)
		return nil, err
	}
	err = yaml.Unmarshal(yamlFile, yamlFileContent)
	if err != nil {
		logger.Error(
			"can't unmarshall yaml file",
			zap.Any("pathToConfig", pathToConfig),
			zap.Error(err),
		)
		return nil, err
	}
	logger.Info("got yaml config values", zap.Any("pathToConfig", pathToConfig))
	return yamlFileContent, nil
}
