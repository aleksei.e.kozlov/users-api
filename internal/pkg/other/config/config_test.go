package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/suite"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
)

type testSuiteConfig struct {
	suite.Suite

	logger *zap.Logger
}

func (s *testSuiteConfig) SetupTest() {
	s.logger = zaptest.NewLogger(s.T())
}

func (s *testSuiteConfig) TearDownTest() {
	// empty yet
}

func TestSuiteConfig(t *testing.T) {
	suite.Run(t, new(testSuiteConfig))
}

func (s *testSuiteConfig) Test_ReadFromExampleYaml() {
	// arrange
	pathToConfig := "../../../../.configs/values_local_example.yaml"

	// action
	config := NewConfig(s.logger, pathToConfig)

	// assert
	s.Equal("0.0.0.0:90", config.GrpcServerAddress)
	s.Equal("0.0.0.0:80", config.GrpcGatewayServerAddress)
	s.Equal("./pkg/gen/proto/swagger/users_api/v1", config.SwaggerUiDir)
	s.Equal("/swaggerui/", config.SwaggerHttpPath)
	s.Equal("mongodb://root_dev:root_dev_password@localhost:27017/?maxPoolSize=20&w=majority", config.MongoURI)
	s.Equal("users_db", config.MongoUsersDb)
	s.Equal("users_collection", config.MongoUsersCollection)
	s.Equal(int64(1000), config.ListUsersPageSizeLimit)
	s.Equal("/live/", config.HealthcheckHttpPath)
	s.Equal("users-api", config.KafkaProduceTopic)
	s.Equal("localhost:9092", config.KafkaBroker)
}

func (s *testSuiteConfig) Test_ReadFromENV() {
	// env priority > file priority
	// arrange
	pathToConfig := "../../../../.configs/values_local_example.yaml"
	expected := "my_env_value"
	s.NoError(os.Setenv("USERS_API_GRPC_SERVER_ADDRESS", expected))
	defer func() {
		s.NoError(os.Unsetenv("USERS_API_GRPC_SERVER_ADDRESS"))
	}()

	// action
	config := NewConfig(s.logger, pathToConfig)

	// assert
	s.Equal(expected, config.GrpcServerAddress)
}

func (s *testSuiteConfig) Test_InvalidYamlFile() {
	// empty values
	// arrange
	pathToConfig := "i_am_invalid.yaml"

	// action
	config := NewConfig(s.logger, pathToConfig)

	// assert
	s.Equal("", config.GrpcServerAddress)
}

func (s *testSuiteConfig) Test_OnEmptyYamlPathTryDefaultConfig() {
	// no panics
	// arrange
	pathToConfig := ""

	// action
	config := NewConfig(s.logger, pathToConfig)

	// assert
	s.Equal("", config.GrpcServerAddress)
}
