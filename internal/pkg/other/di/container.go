package di

import (
	"context"
	"net"
	"net/http"
	"time"

	"github.com/Shopify/sarama"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"

	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/other/clockutc"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/other/config"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/other/healthcheck"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/other/objectidgenerator"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/other/userconverter"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/storage/usersmongo"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/transport/grpcserver"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/transport/kafka"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/transport/middleware"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/usecases/userservice"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/usecases/uservalidator"
	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

/*
dependency injection container
Not thread-safe, init all in 1 thread.
To be used in main and tests.
Lazy loading. Instances caching.
*/
type Container struct {
	// context to use in all ctors
	initCtx      context.Context
	logger       *zap.Logger
	config       *config.Config
	pathToConfig string

	grpcServer           *grpc.Server
	grpcListener         net.Listener
	grpcGatewayServer    *http.Server
	objectIDGenerator    *objectidgenerator.ObjectIDGenerator
	userService          userservice.UserServiceProvider
	clockUTC             *clockutc.ClockUTC
	addUserValidator     *uservalidator.UserValidator
	modifyUserValidator  *uservalidator.UserValidator
	usersMongoStorage    *usersmongo.Storage
	mongoClient          *mongo.Client
	healthcheckService   *healthcheck.HealthCheckService
	usersChangesProducer kafka.KafkaProducerProvider
}

func NewContainer(initCtx context.Context, pathToConfig string) *Container {
	container := &Container{
		initCtx:      initCtx,
		pathToConfig: pathToConfig,
	}
	// init basic fields; for simpler usages e.g. c.logger instead of c.GetLogger()
	_ = container.GetLogger()
	_ = container.GetConfig()
	return container
}

func (c *Container) GetLogger() *zap.Logger {
	if c.logger != nil {
		return c.logger
	}

	zapConfig := zap.NewProductionConfig()
	zapConfig.EncoderConfig.EncodeTime = zapcore.TimeEncoder(func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
		enc.AppendString(t.UTC().Format("2006-01-02T15:04:05Z0700"))
		// 2019-08-13T04:39:11Z
	})
	c.logger, _ = zapConfig.Build()
	return c.logger
}

func (c *Container) GetConfig() *config.Config {
	if c.config != nil {
		return c.config
	}

	c.config = config.NewConfig(c.logger, c.pathToConfig)
	return c.config
}

func (c *Container) GetGRPCServer() (*grpc.Server, error) {
	if c.grpcServer != nil {
		return c.grpcServer, nil
	}

	userService, err := c.GetUserService()
	if err != nil {
		c.logger.Error("can't create grpcServer", zap.Error(err))
		return nil, err
	}

	opts := []grpc.ServerOption{}
	// add middleware
	opts = middleware.AddLogging(c.logger, opts)

	c.grpcServer = grpc.NewServer(opts...)
	users_apiv1.RegisterUsersApiServiceServer(
		c.grpcServer,
		grpcserver.NewGRPCServer(
			c.logger,
			userService,
		),
	)
	return c.grpcServer, nil
}

func (c *Container) GetUserService() (userservice.UserServiceProvider, error) {
	if c.userService != nil {
		return c.userService, nil
	}

	usersMongoStorage, err := c.GetUsersMongoStorage()
	if err != nil {
		c.logger.Error("can't create userService", zap.Error(err))
		return nil, err
	}

	kafkaProducer, err := c.GetUsersChangesProducer()
	if err != nil {
		c.logger.Error("can't create userService", zap.Error(err))
		return nil, err
	}

	c.userService = userservice.NewWrapperNotify(
		c.logger,
		userservice.NewUserService(
			c.logger,
			usersMongoStorage,
			c.GetClockUTC(),
			c.GetObjectIDGenerator(),
			c.GetAddUserValidator(),
			userconverter.NewUserConverter(),
			c.GetModifyUserValidator(),
		),
		kafkaProducer,
	)

	return c.userService, nil
}

func (c *Container) GetAddUserValidator() *uservalidator.UserValidator {
	if c.addUserValidator != nil {
		return c.addUserValidator
	}

	c.addUserValidator = uservalidator.NewAddUserValidator()
	return c.addUserValidator
}

func (c *Container) GetModifyUserValidator() *uservalidator.UserValidator {
	if c.modifyUserValidator != nil {
		return c.modifyUserValidator
	}

	c.modifyUserValidator = uservalidator.NewModifyUserValidator()
	return c.modifyUserValidator
}

func (c *Container) GetClockUTC() *clockutc.ClockUTC {
	if c.clockUTC != nil {
		return c.clockUTC
	}

	c.clockUTC = clockutc.NewClockUTC()
	return c.clockUTC
}

func (c *Container) GetGRPCListener() (net.Listener, error) {
	if c.grpcListener != nil {
		return c.grpcListener, nil
	}

	var err error
	c.grpcListener, err = net.Listen("tcp", c.config.GrpcServerAddress)
	if err != nil {
		c.logger.Error("failed to listen:", zap.Error(err))
		return nil, err
	}
	return c.grpcListener, nil
}

func (c *Container) GetGRPCGatewayServer() (*http.Server, error) {
	if c.grpcGatewayServer != nil {
		return c.grpcGatewayServer, nil
	}

	grpcConnection, err := grpc.DialContext(
		c.initCtx,
		c.config.GrpcServerAddress,
		grpc.WithBlock(),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		c.logger.Error("failed to dial server:", zap.Error(err))
		return nil, err
	}

	serveMux := runtime.NewServeMux()
	err = users_apiv1.RegisterUsersApiServiceHandler(c.initCtx, serveMux, grpcConnection)
	if err != nil {
		c.logger.Error("failed to register gateway:", zap.Error(err))
		return nil, err
	}

	// 	serve swagger ui
	// 	based on https://ribice.medium.com/serve-swaggerui-within-your-golang-application-5486748a5ed4
	fileServerHandler := http.FileServer(http.Dir(c.config.SwaggerUiDir))
	err = serveMux.HandlePath(
		"GET",
		c.config.SwaggerHttpPath+"*",
		func(w http.ResponseWriter, r *http.Request, pathParams map[string]string) {
			http.StripPrefix(c.config.SwaggerHttpPath, fileServerHandler).ServeHTTP(w, r)
		},
	)
	if err != nil {
		c.logger.Error("failed to serveMux.HandlePath", zap.Error(err))
		return nil, err
	}

	// healthcheck
	healthcheckService, err := c.GetHealthcheckService()
	if err != nil {
		c.logger.Error("failed to init grpcGatewayServer", zap.Error(err))
		return nil, err
	}
	err = serveMux.HandlePath(
		"GET",
		c.config.HealthcheckHttpPath+"*",
		func(w http.ResponseWriter, r *http.Request, pathParams map[string]string) {
			description, err := healthcheckService.Healthcheck()
			if err != nil {
				http.Error(w, description, http.StatusInternalServerError)
				return
			}
			http.Error(w, description, http.StatusOK)
		},
	)
	if err != nil {
		c.logger.Error("failed to serveMux.HandlePath", zap.Error(err))
		return nil, err
	}

	c.grpcGatewayServer = &http.Server{
		Addr:    c.config.GrpcGatewayServerAddress,
		Handler: serveMux,
	}
	return c.grpcGatewayServer, nil
}

func (c *Container) GetHealthcheckService() (*healthcheck.HealthCheckService, error) {
	if c.healthcheckService != nil {
		return c.healthcheckService, nil
	}

	var err error
	usersMongoStorage, err := c.GetUsersMongoStorage()
	if err != nil {
		c.logger.Error("can't init usersMongoStorage", zap.Error(err))
		return nil, err
	}

	c.healthcheckService = healthcheck.NewHealthCheckService(
		usersMongoStorage,
	)
	return c.healthcheckService, nil
}

func (c *Container) GetObjectIDGenerator() *objectidgenerator.ObjectIDGenerator {
	if c.objectIDGenerator != nil {
		return c.objectIDGenerator
	}

	c.objectIDGenerator = objectidgenerator.NewObjectIDGenerator()
	return c.objectIDGenerator
}

func (c *Container) GetUsersMongoStorage() (*usersmongo.Storage, error) {
	if c.usersMongoStorage != nil {
		return c.usersMongoStorage, nil
	}

	var err error
	mongoClient, err := c.GetMongoClient()
	if err != nil {
		c.logger.Error("can't init usersMongoStorage", zap.Error(err))
		return nil, err
	}

	c.usersMongoStorage = usersmongo.NewStorage(
		c.logger,
		c.config.MongoUsersDb,
		c.config.MongoUsersCollection,
		mongoClient,
		c.config.ListUsersPageSizeLimit,
	)
	return c.usersMongoStorage, nil
}

func (c *Container) GetMongoClient() (*mongo.Client, error) {
	if c.mongoClient != nil {
		return c.mongoClient, nil
	}

	c.logger.Info("starting init of mongo client")
	var err error
	c.mongoClient, err = mongo.Connect(
		c.initCtx,
		options.Client().ApplyURI(c.config.MongoURI),
	)
	if err != nil {
		c.logger.Error("mongo.Connect err", zap.Error(err))
		c.mongoClient = nil
		return nil, err
	}
	if err := c.mongoClient.Ping(c.initCtx, readpref.Primary()); err != nil {
		c.logger.Error("mongo.Ping err", zap.Error(err))
		c.mongoClient = nil
		return nil, err
	}
	// unique email
	_, err = c.mongoClient.
		Database(c.config.MongoUsersDb).
		Collection(c.config.MongoUsersCollection).
		Indexes().
		CreateOne(
			c.initCtx,
			mongo.IndexModel{
				Keys:    bson.D{primitive.E{Key: "email", Value: 1}},
				Options: options.Index().SetUnique(true),
			},
		)
	if err != nil {
		c.logger.Error("can't create mongodb email uniq index", zap.Error(err))
		c.mongoClient = nil
		return nil, err
	}
	// unique id
	_, err = c.mongoClient.
		Database(c.config.MongoUsersDb).
		Collection(c.config.MongoUsersCollection).
		Indexes().
		CreateOne(
			c.initCtx,
			mongo.IndexModel{
				Keys:    bson.D{primitive.E{Key: "id", Value: 1}},
				Options: options.Index().SetUnique(true),
			},
		)
	if err != nil {
		c.logger.Error("can't create mongodb id uniq index", zap.Error(err))
		c.mongoClient = nil
		return nil, err
	}
	c.logger.Info("successful mongo client init")
	return c.mongoClient, nil
}

func (c *Container) GetUsersChangesProducer() (kafka.KafkaProducerProvider, error) {
	if c.usersChangesProducer != nil {
		return c.usersChangesProducer, nil
	}

	config := sarama.NewConfig()
	config.Producer.Return.Errors = false
	config.Producer.Return.Successes = false
	producer, err := sarama.NewAsyncProducer([]string{c.config.KafkaBroker}, config)
	if err != nil {
		c.logger.Error("can't init kafka producer", zap.Error(err))
		return nil, err
	}
	c.logger.Info("kafka async producer is up", zap.Any("broker", c.config.KafkaBroker))
	c.usersChangesProducer = kafka.NewUsersChangesProducer(
		c.logger,
		producer,
		c.config.KafkaProduceTopic,
	)
	return c.usersChangesProducer, nil
}

func (c *Container) SetUsersChangesProducer(newValue kafka.KafkaProducerProvider) {
	c.usersChangesProducer = newValue
}
