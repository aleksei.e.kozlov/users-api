package userservice

import (
	"context"
	"errors"
	"testing"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/model"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/usecases/userservice/mocks"
	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
)

type testSuiteUserService struct {
	suite.Suite

	logger *zap.Logger

	mockUserStorage         *mocks.UserStorageProvider
	mockClock               *mocks.ClockUTCProvider
	mockObjectIDProvider    *mocks.ObjectIDProvider
	mockAddUserValidator    *mocks.UserValidatorProvider
	mockUserConverter       *mocks.UserConverterProvider
	mockModifyUserValidator *mocks.UserValidatorProvider
	userService             *UserService
}

func (s *testSuiteUserService) SetupTest() {
	s.logger = zaptest.NewLogger(s.T())

	s.mockUserStorage = &mocks.UserStorageProvider{}
	s.mockClock = &mocks.ClockUTCProvider{}
	s.mockObjectIDProvider = &mocks.ObjectIDProvider{}
	s.mockAddUserValidator = &mocks.UserValidatorProvider{}
	s.mockUserConverter = &mocks.UserConverterProvider{}
	s.mockModifyUserValidator = &mocks.UserValidatorProvider{}
	s.userService = NewUserService(
		s.logger,
		s.mockUserStorage,
		s.mockClock,
		s.mockObjectIDProvider,
		s.mockAddUserValidator,
		s.mockUserConverter,
		s.mockModifyUserValidator,
	)
}

func (s *testSuiteUserService) TearDownTest() {
	s.mockUserStorage.AssertExpectations(s.T())
	s.mockClock.AssertExpectations(s.T())
	s.mockObjectIDProvider.AssertExpectations(s.T())
	s.mockAddUserValidator.AssertExpectations(s.T())
	s.mockUserConverter.AssertExpectations(s.T())
	s.mockModifyUserValidator.AssertExpectations(s.T())
}

func TestSuiteUserService(t *testing.T) {
	suite.Run(t, new(testSuiteUserService))
}

func (s *testSuiteUserService) Test_AddUser_CallStorage() {
	// arrange
	inputUserMainValues := &users_apiv1.UserMainValues{
		FirstName: "bob",
		LastName:  "alice",
		Nickname:  "alice_bob",
		Password:  "123",
		Email:     "bob@alice.com",
		Country:   "AliceLand",
	}

	now := time.Now().UTC()
	s.mockClock.On("NowUTC").Return(now)

	expectedObjectID := uuid.FromStringOrNil("123e4567-e89b-12d3-a456-426614174000")
	s.mockObjectIDProvider.On("NewObjectId").Return(expectedObjectID)

	expectedModelUser := &model.User{
		ID:        expectedObjectID,
		FirstName: "bob",
		LastName:  "alice",
		Nickname:  "alice_bob",
		Password:  "123",
		Email:     "bob@alice.com",
		Country:   "AliceLand",
		CreatedAt: now,
		UpdatedAt: now,
	}
	s.mockAddUserValidator.
		On("Validate", expectedModelUser).
		Return(nil)
	s.mockUserStorage.
		On("AddUser", mock.Anything, expectedModelUser).
		Return(nil)

	expectedUser := &users_apiv1.User{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: "expected",
		},
	}
	s.mockUserConverter.On("ModelToGRPC", expectedModelUser).Return(expectedUser)

	// action
	actualUser, err := s.userService.AddUser(
		context.Background(),
		inputUserMainValues,
	)

	// assert
	s.NoError(err)
	s.Equal(expectedUser, actualUser)
}

func (s *testSuiteUserService) Test_AddUser_NilInput() {
	// arrange
	var inputUserMainValues *users_apiv1.UserMainValues

	// action
	actualUser, err := s.userService.AddUser(
		context.Background(),
		inputUserMainValues,
	)

	// assert
	s.Error(err)
	s.Nil(actualUser)
}

func (s *testSuiteUserService) Test_AddUser_ForwardStorageErr() {
	// arrange
	expectedErr := errors.New("my_err")
	inputUserMainValues := &users_apiv1.UserMainValues{
		FirstName: "bob",
		LastName:  "alice",
		Nickname:  "alice_bob",
		Password:  "123",
		Email:     "bob@alice.com",
		Country:   "AliceLand",
	}

	now := time.Now().UTC()
	s.mockClock.On("NowUTC").Return(now)

	expectedObjectID := uuid.FromStringOrNil("123e4567-e89b-12d3-a456-426614174000")
	s.mockObjectIDProvider.On("NewObjectId").Return(expectedObjectID)

	expectedModelUser := &model.User{
		ID:        expectedObjectID,
		FirstName: "bob",
		LastName:  "alice",
		Nickname:  "alice_bob",
		Password:  "123",
		Email:     "bob@alice.com",
		Country:   "AliceLand",
		CreatedAt: now,
		UpdatedAt: now,
	}
	s.mockAddUserValidator.
		On("Validate", expectedModelUser).
		Return(nil)
	s.mockUserStorage.
		On("AddUser", mock.Anything, expectedModelUser).
		Return(expectedErr)

	// action
	actualUser, err := s.userService.AddUser(
		context.Background(),
		inputUserMainValues,
	)

	// assert
	s.Equal(expectedErr, err)
	s.Nil(actualUser)
}

func (s *testSuiteUserService) Test_AddUser_ValidationErr() {
	// arrange
	expectedErr := errors.New("validation_err")
	inputUserMainValues := &users_apiv1.UserMainValues{
		FirstName: "bob",
		LastName:  "alice",
		Nickname:  "alice_bob",
		Password:  "123",
		Email:     "bob@alice.com",
		Country:   "AliceLand",
	}

	now := time.Now().UTC()
	s.mockClock.On("NowUTC").Return(now)

	expectedObjectID := uuid.FromStringOrNil("123e4567-e89b-12d3-a456-426614174000")
	s.mockObjectIDProvider.On("NewObjectId").Return(expectedObjectID)

	expectedModelUser := &model.User{
		ID:        expectedObjectID,
		FirstName: "bob",
		LastName:  "alice",
		Nickname:  "alice_bob",
		Password:  "123",
		Email:     "bob@alice.com",
		Country:   "AliceLand",
		CreatedAt: now,
		UpdatedAt: now,
	}
	s.mockAddUserValidator.
		On("Validate", expectedModelUser).
		Return(expectedErr)

	// action
	actualUser, err := s.userService.AddUser(
		context.Background(),
		inputUserMainValues,
	)

	// assert
	s.Equal(expectedErr, err)
	s.Nil(actualUser)
}

func (s *testSuiteUserService) Test_ListUsers_CallStorage() {
	// arrange
	request := &users_apiv1.ListUsersRequest{
		Page: &users_apiv1.ListUserPage{
			Number: 1,
			Size:   2,
		},
		UserFilter: &users_apiv1.User{
			UserMainValues: &users_apiv1.UserMainValues{
				FirstName: "alice",
			},
		},
	}
	expectedFilterModelUser := &model.User{
		FirstName: "alice",
	}
	s.mockUserConverter.On("GRPCToModel", request.UserFilter).Return(expectedFilterModelUser, nil)

	expectedModelUser1 := &model.User{
		FirstName: "alice1",
	}
	expectedGrpcUser1 := &users_apiv1.User{UserMainValues: &users_apiv1.UserMainValues{
		FirstName: "alice1",
	}}
	s.mockUserConverter.On("ModelToGRPC", expectedModelUser1).Return(expectedGrpcUser1)

	expectedModelUser2 := &model.User{
		FirstName: "alice2",
	}
	expectedGrpcUser2 := &users_apiv1.User{UserMainValues: &users_apiv1.UserMainValues{
		FirstName: "alice2",
	}}
	s.mockUserConverter.On("ModelToGRPC", expectedModelUser2).Return(expectedGrpcUser2)

	s.mockUserStorage.On(
		"ListUsers",
		mock.Anything,
		uint32(1),
		uint32(2),
		expectedFilterModelUser,
	).Return(
		&model.ListUserResponse{
			Users: []*model.User{expectedModelUser1, expectedModelUser2},
			NextPage: &model.ListUserPage{
				Page:     2,
				PageSize: 2,
			},
			PreviousPage: nil,
		},
		nil,
	)
	expectedResponse := &users_apiv1.ListUsersResponse{
		Users: []*users_apiv1.User{expectedGrpcUser1, expectedGrpcUser2},
		NextPage: &users_apiv1.ListUserPage{
			Number: 2,
			Size:   2,
		},
		PreviousPage: nil,
	}

	// action
	actualResponse, err := s.userService.ListUsers(
		context.Background(),
		request,
	)

	// assert
	s.NoError(err)
	s.Equal(expectedResponse, actualResponse)
}

func (s *testSuiteUserService) Test_ListUsers_ForwardStorageErr() {
	// arrange
	expectedErr := errors.New("storage_err")
	request := &users_apiv1.ListUsersRequest{
		Page: &users_apiv1.ListUserPage{
			Number: 1,
			Size:   2,
		},
		UserFilter: &users_apiv1.User{
			UserMainValues: &users_apiv1.UserMainValues{
				FirstName: "alice",
			},
		},
	}
	expectedFilterModelUser := &model.User{
		FirstName: "alice",
	}
	s.mockUserConverter.On("GRPCToModel", request.UserFilter).Return(expectedFilterModelUser, nil)

	s.mockUserStorage.On(
		"ListUsers",
		mock.Anything,
		uint32(1),
		uint32(2),
		expectedFilterModelUser,
	).Return(
		nil,
		expectedErr,
	)

	// action
	actualResponse, err := s.userService.ListUsers(
		context.Background(),
		request,
	)

	// assert
	s.Error(err)
	s.Equal(expectedErr, err)
	s.Nil(actualResponse)
}

func (s *testSuiteUserService) Test_DeleteUser_ProxyToStorage() {
	// arrange
	request := &users_apiv1.DeleteUserRequest{
		Id: "asd",
	}
	expected := &users_apiv1.DeleteUserResponse{}
	s.mockUserStorage.On("DeleteUser", mock.Anything, uuid.FromStringOrNil(request.Id)).Return(nil)

	// action
	actual, err := s.userService.DeleteUser(context.Background(), request)

	// assert
	s.NoError(err)
	s.Equal(expected, actual)
}

func (s *testSuiteUserService) Test_DeleteUser_ProxyStorageErr() {
	// arrange
	expectedErr := errors.New("storage_err")
	request := &users_apiv1.DeleteUserRequest{
		Id: "asd",
	}
	s.mockUserStorage.On("DeleteUser", mock.Anything, uuid.FromStringOrNil(request.Id)).Return(expectedErr)

	// action
	actual, err := s.userService.DeleteUser(context.Background(), request)

	// assert
	s.Error(err)
	s.Nil(actual)
}

func (s *testSuiteUserService) Test_DeleteUser_IgnoreEmptyID() {
	// arrange
	request := &users_apiv1.DeleteUserRequest{
		Id: "", // !
	}

	// action
	actual, err := s.userService.DeleteUser(context.Background(), request)

	// assert
	s.Error(err)
	s.Nil(actual)
}

func (s *testSuiteUserService) Test_DeleteUser_IgnoreEmptyRequest() {
	// arrange
	var request *users_apiv1.DeleteUserRequest

	// action
	actual, err := s.userService.DeleteUser(context.Background(), request)

	// assert
	s.Error(err)
	s.Nil(actual)
}

func (s *testSuiteUserService) Test_ModifyUser_Success() {
	// arrange
	request := &users_apiv1.ModifyUserRequest{
		Id: "asd",
		NewMainValues: &users_apiv1.UserMainValues{
			FirstName: "myFirstName",
			LastName:  "myLastName",
			Nickname:  "myNickname",
			Password:  "myPassword",
			Email:     "myEmail",
			Country:   "myCountry",
		},
	}
	now := time.Now().UTC()
	s.mockClock.On("NowUTC").Return(now)
	expectedModelUser := &model.User{
		ID:        uuid.FromStringOrNil(request.Id),
		FirstName: request.NewMainValues.FirstName,
		LastName:  request.NewMainValues.LastName,
		Nickname:  request.NewMainValues.Nickname,
		Password:  request.NewMainValues.Password,
		Email:     request.NewMainValues.Email,
		Country:   request.NewMainValues.Country,
		CreatedAt: time.Time{},
		UpdatedAt: now,
	}
	expectedModelUser.TruncateToSecondsTimeFields()
	s.mockModifyUserValidator.On("Validate", expectedModelUser).Return(nil)
	modelUserInStorage := &model.User{FirstName: "myName2"}
	s.mockUserStorage.
		On(
			"ListUsers",
			mock.Anything,
			uint32(0),
			uint32(1),
			&model.User{
				ID: uuid.FromStringOrNil(request.Id),
			},
		).
		Return(
			&model.ListUserResponse{
				Users: []*model.User{modelUserInStorage},
			},
			nil,
		)
	expectedGrpcUser := &users_apiv1.User{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: "myFirstName",
		},
	}
	s.mockUserConverter.On("ModelToGRPC", expectedModelUser).Return(
		expectedGrpcUser,
	)
	grpcUserInStorage := &users_apiv1.User{
		UserMainValues: &users_apiv1.UserMainValues{
			FirstName: "myName2",
		},
	}
	s.mockUserConverter.On("ModelToGRPC", modelUserInStorage).Return(
		grpcUserInStorage,
	)
	s.mockUserStorage.
		On(
			"ModifyUser",
			mock.Anything,
			expectedModelUser,
		).Return(nil)
	expected := &users_apiv1.ModifyUserResponse{
		OldUser:      grpcUserInStorage,
		ModifiedUser: expectedGrpcUser,
	}

	// action
	actual, err := s.userService.ModifyUser(context.Background(), request)

	// assert
	s.NoError(err)
	s.Equal(expected, actual)
}
