package userservice

import (
	"context"

	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
	"go.uber.org/zap"
)

/*
full proxy to UserService, but with notification on successful users modifications
*/
type WrapperNotify struct {
	logger *zap.Logger

	innerUserService UserServiceProvider
	notifier         UsersChangesNotifyProvider
}

func NewWrapperNotify(
	logger *zap.Logger,
	innerUserService UserServiceProvider,
	notifier UsersChangesNotifyProvider,
) *WrapperNotify {
	return &WrapperNotify{
		logger:           logger,
		innerUserService: innerUserService,
		notifier:         notifier,
	}
}

func (w *WrapperNotify) AddUser(
	ctx context.Context,
	userMainValues *users_apiv1.UserMainValues,
) (*users_apiv1.User, error) {
	user, err := w.innerUserService.AddUser(
		ctx,
		userMainValues,
	)
	if err == nil {
		errNotifier := w.notifier.NotifyAddUser(ctx, user)
		if errNotifier != nil {
			w.logger.Error("AddUser notify error, will ignore it", zap.Error(errNotifier))
		}
	}
	return user, err
}

func (w *WrapperNotify) ListUsers(
	ctx context.Context,
	request *users_apiv1.ListUsersRequest,
) (*users_apiv1.ListUsersResponse, error) {
	return w.innerUserService.ListUsers(ctx, request)
}

func (w *WrapperNotify) DeleteUser(
	ctx context.Context,
	request *users_apiv1.DeleteUserRequest,
) (*users_apiv1.DeleteUserResponse, error) {
	response, err := w.innerUserService.DeleteUser(ctx, request)
	if err == nil {
		errNotifier := w.notifier.NotifyDeleteUser(ctx, request.Id)
		if errNotifier != nil {
			w.logger.Error("NotifyDeleteUser err, will ignore it", zap.Error(errNotifier))
		}
	}
	return response, err
}

func (w *WrapperNotify) ModifyUser(
	ctx context.Context,
	request *users_apiv1.ModifyUserRequest,
) (*users_apiv1.ModifyUserResponse, error) {
	response, err := w.innerUserService.ModifyUser(ctx, request)
	if err == nil {
		errNotifier := w.notifier.NotifyModifyUser(
			ctx,
			response.OldUser,
			response.ModifiedUser,
		)
		if errNotifier != nil {
			w.logger.Error("NotifyModifyUser err, will ignore it", zap.Error(errNotifier))
		}
	}
	return response, err
}
