package userservice

import (
	"context"
	"errors"
	"fmt"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/model"
	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
	"go.uber.org/zap"
)

type UserService struct {
	logger *zap.Logger

	userStorage         UserStorageProvider
	clockUTC            ClockUTCProvider
	objectIDProvider    ObjectIDProvider
	addUserValidator    UserValidatorProvider
	userConverter       UserConverterProvider
	modifyUserValidator UserValidatorProvider
}

func NewUserService(
	logger *zap.Logger,
	userStorage UserStorageProvider,
	clockUTC ClockUTCProvider,
	objectIDProvider ObjectIDProvider,
	addUserValidator UserValidatorProvider,
	userConverter UserConverterProvider,
	modifyUserValidator UserValidatorProvider,
) *UserService {
	return &UserService{
		logger:              logger,
		userStorage:         userStorage,
		clockUTC:            clockUTC,
		objectIDProvider:    objectIDProvider,
		addUserValidator:    addUserValidator,
		userConverter:       userConverter,
		modifyUserValidator: modifyUserValidator,
	}
}

func (s *UserService) AddUser(
	ctx context.Context,
	userMainValues *users_apiv1.UserMainValues,
) (*users_apiv1.User, error) {
	if userMainValues == nil {
		return nil, errors.New("nil userMainValues")
	}

	now := s.clockUTC.NowUTC()
	userToInsert := &model.User{
		ID:        s.objectIDProvider.NewObjectId(),
		FirstName: userMainValues.FirstName,
		LastName:  userMainValues.LastName,
		Nickname:  userMainValues.Nickname,
		Password:  userMainValues.Password,
		Email:     userMainValues.Email,
		Country:   userMainValues.Country,
		CreatedAt: now,
		UpdatedAt: now,
	}
	err := s.addUserValidator.Validate(userToInsert)
	if err != nil {
		s.logger.Info("AddUser validation failed", zap.Error(err))
		return nil, err
	}

	err = s.userStorage.AddUser(
		ctx,
		userToInsert,
	)
	if err != nil {
		s.logger.Error("storage can't AddUser", zap.Error(err))
		return nil, err
	}
	responseUser := s.userConverter.ModelToGRPC(userToInsert)
	return responseUser, nil
}

func (s *UserService) ListUsers(
	ctx context.Context,
	request *users_apiv1.ListUsersRequest,
) (*users_apiv1.ListUsersResponse, error) {
	if request.Page == nil {
		return nil, errors.New("nil Page")
	}

	// fill filter defaults if needed
	if request.UserFilter == nil {
		request.UserFilter = &users_apiv1.User{}
	}
	if request.UserFilter.UserGeneratedValues == nil {
		request.UserFilter.UserGeneratedValues = &users_apiv1.UserGeneratedValues{}
	}
	if request.UserFilter.UserMainValues == nil {
		request.UserFilter.UserMainValues = &users_apiv1.UserMainValues{}
	}
	modelUserFilter, err := s.userConverter.GRPCToModel(request.UserFilter)
	if err != nil {
		s.logger.Error("GRPCToModel err", zap.Error(err))
		return nil, err
	}

	storageResponse, err := s.userStorage.ListUsers(
		ctx,
		request.Page.Number,
		request.Page.Size,
		modelUserFilter,
	)
	if err != nil {
		s.logger.Error("ListUsers got error", zap.Error(err))
		return nil, err
	}

	response := &users_apiv1.ListUsersResponse{
		Users:        []*users_apiv1.User{},
		NextPage:     nil,
		PreviousPage: nil,
	}
	for _, modelUser := range storageResponse.Users {
		response.Users = append(
			response.Users,
			s.userConverter.ModelToGRPC(modelUser),
		)
	}
	if storageResponse.NextPage != nil {
		response.NextPage = &users_apiv1.ListUserPage{
			Number: storageResponse.NextPage.Page,
			Size:   storageResponse.NextPage.PageSize,
		}
	}
	if storageResponse.PreviousPage != nil {
		response.PreviousPage = &users_apiv1.ListUserPage{
			Number: storageResponse.PreviousPage.Page,
			Size:   storageResponse.PreviousPage.PageSize,
		}
	}
	return response, nil
}

func (s *UserService) DeleteUser(
	ctx context.Context,
	request *users_apiv1.DeleteUserRequest,
) (*users_apiv1.DeleteUserResponse, error) {
	if request == nil {
		return nil, errors.New("empty request")
	}
	if len(request.Id) == 0 {
		return nil, errors.New("can't delete by empty id")
	}

	err := s.userStorage.DeleteUser(ctx, uuid.FromStringOrNil(request.Id))
	if err != nil {
		s.logger.Error("can't DeleteUser", zap.Error(err))
		return nil, err
	}
	return &users_apiv1.DeleteUserResponse{}, nil
}

func (s *UserService) ModifyUser(
	ctx context.Context,
	request *users_apiv1.ModifyUserRequest,
) (*users_apiv1.ModifyUserResponse, error) {
	if request == nil {
		return nil, errors.New("nil request")
	}
	if request.NewMainValues == nil {
		return nil, errors.New("nil NewMainValues")
	}

	now := s.clockUTC.NowUTC()
	modifiedModelUser := &model.User{
		ID:        uuid.FromStringOrNil(request.Id),
		FirstName: request.NewMainValues.FirstName,
		LastName:  request.NewMainValues.LastName,
		Nickname:  request.NewMainValues.Nickname,
		Password:  request.NewMainValues.Password,
		Email:     request.NewMainValues.Email,
		Country:   request.NewMainValues.Country,
		CreatedAt: time.Time{}, // leave empty; bson filters will take care to preserve old value
		UpdatedAt: now,
	}
	// to remove nanoseconds from response
	modifiedModelUser.TruncateToSecondsTimeFields()

	err := s.modifyUserValidator.Validate(modifiedModelUser)
	if err != nil {
		s.logger.Error("can't ModifyUser, validator fails", zap.Error(err))
		return nil, err
	}

	// check if changes really change users' main values
	listResponse, err := s.userStorage.ListUsers(ctx, 0, 1, &model.User{
		ID: uuid.FromStringOrNil(request.Id),
	})
	if err != nil {
		s.logger.Error("can't compare new values with old on modify", zap.Error(err))
		return nil, err
	}
	var currentGrpcUser *users_apiv1.User
	var modifiedGrpcUser *users_apiv1.User
	if len(listResponse.Users) > 0 {
		currentModelUser := listResponse.Users[0]
		currentGrpcUser = s.userConverter.ModelToGRPC(currentModelUser)
		modifiedGrpcUser = s.userConverter.ModelToGRPC(modifiedModelUser)
		if currentGrpcUser.UserMainValues.String() == modifiedGrpcUser.UserMainValues.String() {
			return nil, fmt.Errorf("nothing to modify because all fields are the same; id=%s", request.Id)
		}

		// to fill in response missed createdAt value
		if modifiedGrpcUser.UserGeneratedValues != nil && currentGrpcUser.UserGeneratedValues != nil {
			modifiedGrpcUser.UserGeneratedValues.CreatedAt = currentGrpcUser.UserGeneratedValues.CreatedAt
		}
	} else {
		return nil, fmt.Errorf("not found a user to modify; id=%s", request.Id)
	}

	err = s.userStorage.ModifyUser(ctx, modifiedModelUser)
	if err != nil {
		s.logger.Error("can't ModifyUser, storage err", zap.Error(err))
		return nil, err
	}
	return &users_apiv1.ModifyUserResponse{
		OldUser:      currentGrpcUser,
		ModifiedUser: modifiedGrpcUser,
	}, nil
}
