//go:generate mockery --case=underscore --name UserStorageProvider
//go:generate mockery --case=underscore --name ClockUTCProvider
//go:generate mockery --case=underscore --name ObjectIDProvider
//go:generate mockery --case=underscore --name UserValidatorProvider
//go:generate mockery --case=underscore --name UserConverterProvider
//go:generate mockery --case=underscore --name UserServiceProvider
//go:generate mockery --case=underscore --name UsersChangesNotifyProvider

package userservice

import (
	"context"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/model"
	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
)

type UserStorageProvider interface {
	AddUser(context.Context, *model.User) error
	ListUsers(
		ctx context.Context,
		page uint32,
		pageSize uint32,
		userFilter *model.User,
	) (*model.ListUserResponse, error)
	DeleteUser(
		ctx context.Context,
		id uuid.UUID,
	) error
	ModifyUser(context.Context, *model.User) error
}

type ClockUTCProvider interface {
	NowUTC() time.Time
}

type ObjectIDProvider interface {
	NewObjectId() uuid.UUID
}

type UserValidatorProvider interface {
	Validate(*model.User) error
}

type UserConverterProvider interface {
	ModelToGRPC(*model.User) *users_apiv1.User
	GRPCToModel(*users_apiv1.User) (*model.User, error)
}

type UserServiceProvider interface {
	AddUser(
		ctx context.Context,
		userMainValues *users_apiv1.UserMainValues,
	) (*users_apiv1.User, error)
	ListUsers(
		ctx context.Context,
		request *users_apiv1.ListUsersRequest,
	) (*users_apiv1.ListUsersResponse, error)
	DeleteUser(
		ctx context.Context,
		request *users_apiv1.DeleteUserRequest,
	) (*users_apiv1.DeleteUserResponse, error)
	ModifyUser(
		ctx context.Context,
		request *users_apiv1.ModifyUserRequest,
	) (*users_apiv1.ModifyUserResponse, error)
}

type UsersChangesNotifyProvider interface {
	NotifyAddUser(
		ctx context.Context,
		user *users_apiv1.User,
	) error
	NotifyDeleteUser(
		ctx context.Context,
		id string,
	) error
	NotifyModifyUser(
		ctx context.Context,
		oldUser *users_apiv1.User,
		modifiedUser *users_apiv1.User,
	) error
}
