package userservice_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/usecases/userservice"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/usecases/userservice/mocks"
	users_apiv1 "gitlab.com/aleksei.e.kozlov/users-api/pkg/gen/proto/go/users_api/v1"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
)

type testSuiteWrapperNotify struct {
	suite.Suite

	logger *zap.Logger

	mockInnerUserService *mocks.UserServiceProvider
	mockNotifier         *mocks.UsersChangesNotifyProvider
	wrapperNotify        *userservice.WrapperNotify
}

func (s *testSuiteWrapperNotify) SetupTest() {
	s.logger = zaptest.NewLogger(s.T())

	s.mockInnerUserService = &mocks.UserServiceProvider{}
	s.mockNotifier = &mocks.UsersChangesNotifyProvider{}
	s.wrapperNotify = userservice.NewWrapperNotify(
		s.logger,
		s.mockInnerUserService,
		s.mockNotifier,
	)
}

func (s *testSuiteWrapperNotify) TearDownTest() {
	s.mockInnerUserService.AssertExpectations(s.T())
	s.mockNotifier.AssertExpectations(s.T())
}

func TestSuiteWrapperNotify(t *testing.T) {
	suite.Run(t, new(testSuiteWrapperNotify))
}

func (s *testSuiteWrapperNotify) Test_AddUser_NotifyOnSuccess() {
	// arrange
	userMainValues := &users_apiv1.UserMainValues{
		FirstName: "asd",
	}
	expectedUser := &users_apiv1.User{
		UserGeneratedValues: &users_apiv1.UserGeneratedValues{
			Id: "my_id",
		},
	}
	s.mockInnerUserService.On(
		"AddUser",
		mock.Anything,
		userMainValues,
	).Return(expectedUser, nil)
	s.mockNotifier.On(
		"NotifyAddUser",
		mock.Anything,
		expectedUser,
	).Return(errors.New("notifier is broken"))

	// action
	actualUser, err := s.wrapperNotify.AddUser(
		context.Background(),
		userMainValues,
	)

	// assert
	s.NoError(err)
	s.Equal(expectedUser, actualUser)
}

func (s *testSuiteWrapperNotify) Test_AddUser_DoNotNotifyOnErr() {
	// arrange
	expectedInnerErr := errors.New("inner service err")
	userMainValues := &users_apiv1.UserMainValues{
		FirstName: "asd",
	}
	s.mockInnerUserService.On(
		"AddUser",
		mock.Anything,
		userMainValues,
	).Return(nil, expectedInnerErr)

	// action
	actualUser, err := s.wrapperNotify.AddUser(
		context.Background(),
		userMainValues,
	)

	// assert
	s.Error(err)
	s.Nil(actualUser)
}

func (s *testSuiteWrapperNotify) Test_ListUsers_ProxyAsIs() {
	// arrange
	request := &users_apiv1.ListUsersRequest{
		Page: &users_apiv1.ListUserPage{
			Number: 1,
		},
	}
	expectedResponse := &users_apiv1.ListUsersResponse{
		NextPage: &users_apiv1.ListUserPage{Number: 22},
	}
	expectedErr := errors.New("inner_service_err")
	s.mockInnerUserService.On(
		"ListUsers",
		mock.Anything,
		request,
	).Return(expectedResponse, expectedErr)

	// action
	actualResponse, err := s.wrapperNotify.ListUsers(
		context.Background(),
		request,
	)

	// assert
	s.Equal(expectedErr, err)
	s.Equal(expectedResponse, actualResponse)
}

func (s *testSuiteWrapperNotify) Test_DeleteUser_NotifyOnSuccess() {
	// arrange
	request := &users_apiv1.DeleteUserRequest{
		Id: "32131",
	}
	expectedResponse := &users_apiv1.DeleteUserResponse{}
	s.mockInnerUserService.On(
		"DeleteUser",
		mock.Anything,
		request,
	).Return(expectedResponse, nil)
	s.mockNotifier.On(
		"NotifyDeleteUser",
		mock.Anything,
		request.Id,
	).Return(errors.New("notifier is broken"))

	// action
	actualResponse, err := s.wrapperNotify.DeleteUser(
		context.Background(),
		request,
	)

	// assert
	s.NoError(err)
	s.Equal(expectedResponse, actualResponse)
}

func (s *testSuiteWrapperNotify) Test_DeleteUser_DoNotNotifyOnErr() {
	// arrange
	expectedErr := errors.New("inner service err")
	request := &users_apiv1.DeleteUserRequest{
		Id: "32131",
	}
	s.mockInnerUserService.On(
		"DeleteUser",
		mock.Anything,
		request,
	).Return(nil, expectedErr)

	// action
	actualResponse, err := s.wrapperNotify.DeleteUser(
		context.Background(),
		request,
	)

	// assert
	s.Error(err)
	s.Nil(actualResponse)
}

func (s *testSuiteWrapperNotify) Test_ModifyUser_NotifyOnSuccess() {
	// arrange
	request := &users_apiv1.ModifyUserRequest{
		Id: "123123",
	}
	expectedResponse := &users_apiv1.ModifyUserResponse{
		OldUser: &users_apiv1.User{UserGeneratedValues: &users_apiv1.UserGeneratedValues{Id: "123123"}},
	}
	s.mockInnerUserService.On(
		"ModifyUser",
		mock.Anything,
		request,
	).Return(expectedResponse, nil)
	s.mockNotifier.On(
		"NotifyModifyUser",
		mock.Anything,
		expectedResponse.OldUser,
		expectedResponse.ModifiedUser,
	).Return(errors.New("notifier err"))

	// action
	actualResponse, err := s.wrapperNotify.ModifyUser(context.Background(), request)

	// assert
	s.NoError(err)
	s.Equal(expectedResponse, actualResponse)
}

func (s *testSuiteWrapperNotify) Test_ModifyUser_DoNotNotifyOnErr() {
	// arrange
	expectedErr := errors.New("inner service err")
	request := &users_apiv1.ModifyUserRequest{
		Id: "123123",
	}
	s.mockInnerUserService.On(
		"ModifyUser",
		mock.Anything,
		request,
	).Return(nil, expectedErr)

	// action
	actualResponse, err := s.wrapperNotify.ModifyUser(context.Background(), request)

	// assert
	s.Error(err)
	s.Nil(actualResponse)
}
