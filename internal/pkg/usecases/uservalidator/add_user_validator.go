package uservalidator

func NewAddUserValidator() *UserValidator {
	return NewUserValidator(
		idIsValidUUID,
		passwordIsNotEmpty,
		emailIsValid,
		createdAtNotEmpty,
		updatedAtNotEmpty,
	)
}
