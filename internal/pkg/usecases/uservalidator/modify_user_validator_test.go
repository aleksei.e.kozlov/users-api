package uservalidator_test

import (
	"context"
	"testing"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/suite"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/model"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/other/di"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/other/objectidgenerator"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/usecases/uservalidator"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
)

type testSuiteModifyUserValidator struct {
	suite.Suite

	logger      *zap.Logger
	idGenerator *objectidgenerator.ObjectIDGenerator

	validator *uservalidator.UserValidator
}

func (s *testSuiteModifyUserValidator) SetupTest() {
	s.logger = zaptest.NewLogger(s.T())

	diContainer := di.NewContainer(context.Background(), "")
	s.idGenerator = diContainer.GetObjectIDGenerator()

	s.validator = diContainer.GetModifyUserValidator()
}

func (s *testSuiteModifyUserValidator) TearDownTest() {
	// empty yet
}

func TestSuiteModifyUserValidator(t *testing.T) {
	suite.Run(t, new(testSuiteModifyUserValidator))
}

func (s *testSuiteModifyUserValidator) Test_ModifyUser_Rules() {
	// arrange
	validUserId := s.idGenerator.NewObjectId()
	now := time.Now().UTC()
	type args struct {
		user *model.User
	}
	tests := []struct {
		name string
		args args
		pass bool
	}{
		{
			name: "ok",
			args: args{
				user: &model.User{
					ID:        validUserId,
					FirstName: "bob",
					LastName:  "alice",
					Nickname:  "alice_bob",
					Password:  "123",
					Email:     "bob@alice.com",
					Country:   "AliceLand",
					CreatedAt: time.Time{},
					UpdatedAt: now,
				},
			},
			pass: true,
		},
		{
			name: "fail on userid that is not valid UUID",
			args: args{
				user: &model.User{
					ID:        uuid.Nil, // !
					FirstName: "bob",
					LastName:  "alice",
					Nickname:  "alice_bob",
					Password:  "123",
					Email:     "bob@alice.com",
					Country:   "AliceLand",
					CreatedAt: now,
					UpdatedAt: now,
				},
			},
			pass: false,
		},
		{
			name: "fail on empty password",
			args: args{
				user: &model.User{
					ID:        validUserId,
					FirstName: "bob",
					LastName:  "alice",
					Nickname:  "alice_bob",
					Password:  "", // !
					Email:     "bob@alice.com",
					Country:   "AliceLand",
					CreatedAt: now,
					UpdatedAt: now,
				},
			},
			pass: false,
		},
		{
			name: "fail on empty email",
			args: args{
				user: &model.User{
					ID:        validUserId,
					FirstName: "bob",
					LastName:  "alice",
					Nickname:  "alice_bob",
					Password:  "123",
					Email:     "", // !
					Country:   "AliceLand",
					CreatedAt: now,
					UpdatedAt: now,
				},
			},
			pass: false,
		},
		{
			name: "fail on invalid email",
			args: args{
				user: &model.User{
					ID:        validUserId,
					FirstName: "bob",
					LastName:  "alice",
					Nickname:  "alice_bob",
					Password:  "123",
					Email:     "not_email", // !
					Country:   "AliceLand",
					CreatedAt: now,
					UpdatedAt: now,
				},
			},
			pass: false,
		},
		{
			name: "fail on NOT empty CreatedAt",
			args: args{
				user: &model.User{
					ID:        validUserId,
					FirstName: "bob",
					LastName:  "alice",
					Nickname:  "alice_bob",
					Password:  "123",
					Email:     "bob@alice.com",
					Country:   "AliceLand",
					CreatedAt: time.Time{}, // !
					UpdatedAt: now,
				},
			},
			pass: true,
		},
		{
			name: "fail on empty UpdatedAt",
			args: args{
				user: &model.User{
					ID:        validUserId,
					FirstName: "bob",
					LastName:  "alice",
					Nickname:  "alice_bob",
					Password:  "123",
					Email:     "bob@alice.com",
					Country:   "AliceLand",
					CreatedAt: now,
					UpdatedAt: time.Time{}, // !
				},
			},
			pass: false,
		},
	}

	for _, tt := range tests {
		s.Run(tt.name, func() {
			// action
			actualErr := s.validator.Validate(tt.args.user)

			// assert
			if tt.pass {
				s.NoError(actualErr, tt.name, actualErr)
			} else {
				s.Error(actualErr, tt.name, actualErr)
			}
		})
	}
}
