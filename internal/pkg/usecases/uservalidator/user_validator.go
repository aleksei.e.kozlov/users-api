package uservalidator

import "gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/model"

type UserValidatorProvider interface {
	Validate(*model.User) error
}

type userValidateFn func(user *model.User) error

type UserValidator struct {
	validators []userValidateFn
}

func NewUserValidator(validators ...userValidateFn) *UserValidator {
	return &UserValidator{
		validators: validators,
	}
}

func (v *UserValidator) Validate(user *model.User) error {
	for _, validatorFn := range v.validators {
		if err := validatorFn(user); err != nil {
			return err
		}
	}
	return nil
}
