package uservalidator

func NewModifyUserValidator() *UserValidator {
	return NewUserValidator(
		idIsValidUUID,
		passwordIsNotEmpty,
		emailIsValid,
		createdAtIsEmpty,
		updatedAtNotEmpty,
	)
}
