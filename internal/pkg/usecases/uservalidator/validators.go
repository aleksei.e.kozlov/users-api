package uservalidator

import (
	"errors"
	"regexp"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/model"
)

func idIsValidUUID(user *model.User) error {
	if user.ID == uuid.Nil {
		return errors.New("zero uuid is not valid")
	}
	return nil
}

func passwordIsNotEmpty(user *model.User) error {
	if len(user.Password) == 0 {
		return errors.New("empty password")
	}
	return nil
}

func emailIsValid(user *model.User) error {
	// from https://stackoverflow.com/a/67686133
	emailRegex := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	if !emailRegex.MatchString(user.Email) {
		return errors.New("invalid email")
	}
	return nil
}

func createdAtNotEmpty(user *model.User) error {
	if user.CreatedAt.IsZero() {
		return errors.New("empty CreatedAt")
	}
	return nil
}

func createdAtIsEmpty(user *model.User) error {
	if user.CreatedAt.IsZero() {
		return nil
	}
	return errors.New("NOT empty CreatedAt")
}

func updatedAtNotEmpty(user *model.User) error {
	if user.UpdatedAt.IsZero() {
		return errors.New("empty CreatedAt")
	}
	return nil
}
