package model

type ListUserPage struct {
	Page     uint32
	PageSize uint32
}

type ListUserResponse struct {
	Users        []*User
	NextPage     *ListUserPage
	PreviousPage *ListUserPage
}
