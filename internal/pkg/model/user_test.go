package model_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
)

type testSuiteUser struct {
	suite.Suite

	logger *zap.Logger
}

func (s *testSuiteUser) SetupTest() {
	s.logger = zaptest.NewLogger(s.T())
}

func (s *testSuiteUser) TearDownTest() {
	// empty yet
}

func TestSuiteUser(t *testing.T) {
	suite.Run(t, new(testSuiteUser))
}

func (s *testSuiteUser) Test_GenerateBsonFilter_EmptyUser() {
	// arrange
	user := &model.User{}
	expected := bson.M{}

	// action
	actual := user.GenerateBsonSearchFilter()

	// assert
	s.Equal(expected, actual)
}

func (s *testSuiteUser) Test_GenerateBsonFilter_NotEmptyUser() {
	// arrange
	createdAt := time.Now().UTC()
	updatedAt := createdAt.Add(time.Second)
	user := &model.User{
		ID:        [16]byte{0},
		FirstName: "my_firstname",
		LastName:  "my_lastname",
		Nickname:  "my_nickname",
		Password:  "my_password",
		Email:     "my_email",
		Country:   "my_country",
		CreatedAt: createdAt,
		UpdatedAt: updatedAt,
	}
	expected := primitive.M{
		"country":    "my_country",
		"created_at": createdAt,
		"email":      "my_email",
		"first_name": "my_firstname",
		"last_name":  "my_lastname",
		"nickname":   "my_nickname",
		"password":   "my_password",
		"updated_at": updatedAt,
	}

	// action
	actual := user.GenerateBsonSearchFilter()

	// assert
	s.Equal(expected, actual)
}

func (s *testSuiteUser) Test_GenerateBsonUpdateFilter() {
	// arrange
	createdAt := time.Now().UTC()
	updatedAt := createdAt.Add(time.Second)
	user := &model.User{
		ID:        [16]byte{0},
		FirstName: "my_firstname",
		LastName:  "my_lastname",
		Nickname:  "my_nickname",
		Password:  "my_password",
		Email:     "my_email",
		Country:   "my_country",
		CreatedAt: createdAt,
		UpdatedAt: updatedAt,
	}
	expected := primitive.M{
		"first_name": "my_firstname",
		"last_name":  "my_lastname",
		"nickname":   "my_nickname",
		"password":   "my_password",
		"email":      "my_email",
		"country":    "my_country",
		"updated_at": updatedAt,
	}

	// action
	actual := user.GenerateBsonUpdateFilter()

	// assert
	s.Equal(expected, actual)
}
