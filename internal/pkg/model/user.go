package model

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"go.mongodb.org/mongo-driver/bson"
)

type User struct {
	ID        uuid.UUID `bson:"id,omitempty"`
	FirstName string    `bson:"first_name,omitempty"`
	LastName  string    `bson:"last_name,omitempty"`
	Nickname  string    `bson:"nickname,omitempty"`
	Password  string    `bson:"password,omitempty"`
	Email     string    `bson:"email,omitempty"`
	Country   string    `bson:"country,omitempty"`
	CreatedAt time.Time `bson:"created_at,omitempty"`
	UpdatedAt time.Time `bson:"updated_at,omitempty"`
}

func (u *User) TruncateToSecondsTimeFields() {
	u.CreatedAt = u.CreatedAt.Truncate(time.Second)
	u.UpdatedAt = u.UpdatedAt.Truncate(time.Second)
}

func (u *User) GenerateBsonSearchFilter() bson.M {
	defaultUser := &User{}
	findFilter := bson.M{}
	if defaultUser.ID != u.ID {
		findFilter["id"] = u.ID
	}
	if defaultUser.FirstName != u.FirstName {
		findFilter["first_name"] = u.FirstName
	}
	if defaultUser.LastName != u.LastName {
		findFilter["last_name"] = u.LastName
	}
	if defaultUser.Nickname != u.Nickname {
		findFilter["nickname"] = u.Nickname
	}
	if defaultUser.Password != u.Password {
		findFilter["password"] = u.Password
	}
	if defaultUser.Email != u.Email {
		findFilter["email"] = u.Email
	}
	if defaultUser.Country != u.Country {
		findFilter["country"] = u.Country
	}
	if defaultUser.CreatedAt != u.CreatedAt {
		findFilter["created_at"] = u.CreatedAt
	}
	if defaultUser.UpdatedAt != u.UpdatedAt {
		findFilter["updated_at"] = u.UpdatedAt
	}
	return findFilter
}

func (u *User) GenerateBsonUpdateFilter() bson.M {
	updateFilter := bson.M{}
	// updateFilter["id"] = u.ID // ! do not update id
	updateFilter["first_name"] = u.FirstName
	updateFilter["last_name"] = u.LastName
	updateFilter["nickname"] = u.Nickname
	updateFilter["password"] = u.Password
	updateFilter["email"] = u.Email
	updateFilter["country"] = u.Country
	// updateFilter["created_at"] = u.CreatedAt // ! do not update created_at
	updateFilter["updated_at"] = u.UpdatedAt
	return updateFilter
}
