package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/aleksei.e.kozlov/users-api/internal/pkg/other/di"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

func main() {
	initCtx, initCancelFn := context.WithTimeout(context.Background(), 5*time.Second)
	defer initCancelFn()
	diContainer := di.NewContainer(initCtx, "")

	logger := diContainer.GetLogger()
	defer func() {
		_ = logger.Sync() // flushes buffer, if any
	}()
	config := diContainer.GetConfig()

	errGroup := &errgroup.Group{}

	// grpc server
	grpcListener, err := diContainer.GetGRPCListener()
	if err != nil {
		logger.Fatal("failed to init grpcListener", zap.Error(err))
	}
	grpcServer, err := diContainer.GetGRPCServer()
	if err != nil {
		logger.Fatal("failed to init grpcServer", zap.Error(err))
	}
	errGroup.Go(func() error {
		logger.Info("serving gRPC", zap.Any("grpcListener.Addr", grpcListener.Addr()))
		return grpcServer.Serve(grpcListener)
	})

	// grpc gateway server
	grpcGatewayServer, err := diContainer.GetGRPCGatewayServer()
	if err != nil {
		logger.Fatal("failed to init grpcGatewayServer", zap.Error(err))
	}
	errGroup.Go(func() error {
		logger.Sugar().Infof("serving gRPC-Gateway on http://%s", config.GrpcGatewayServerAddress)
		logger.Sugar().Infof("serving swagger ui on http://%s%s", config.GrpcGatewayServerAddress, config.SwaggerHttpPath)
		logger.Sugar().Infof("serving healthcheck on http://%s%s", config.GrpcGatewayServerAddress, config.HealthcheckHttpPath)
		err = grpcGatewayServer.ListenAndServe()
		if err == http.ErrServerClosed {
			return nil
		} else {
			return err
		}
	})

	// kafka producer
	kafkaProducer, err := diContainer.GetUsersChangesProducer()
	if err != nil {
		logger.Fatal("can't init kafkaProducer", zap.Error(err))
	}

	// catch system signals for graceful shutdown
	exitSignalCh := make(chan os.Signal, 1)
	signal.Notify(exitSignalCh, os.Interrupt, syscall.SIGTERM)
	errGroup.Go(func() error {
		signal := <-exitSignalCh
		logger.Info(fmt.Sprintf("got signal %v, attempting graceful shutdown", signal))

		grpcServer.GracefulStop()
		logger.Info("grpcServer is closed")

		err := kafkaProducer.Close()
		if err != nil {
			logger.Error("can't close kafka producer", zap.Error(err))
			return err
		}
		logger.Info("kafkaProducer is closed")

		ctxShutdown, cancelFuncShutdown := context.WithTimeout(context.Background(), time.Second)
		defer cancelFuncShutdown()
		err = grpcGatewayServer.Shutdown(ctxShutdown)
		if err != nil {
			logger.Error("can't close grpcGatewayServer", zap.Error(err))
		}
		logger.Info("grpcGatewayServer is closed")
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		logger.Fatal("main got error", zap.Error(err))
	}
	logger.Info("all main goroutines are done, bye.")
}
