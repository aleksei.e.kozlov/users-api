############ base_container ############
FROM golang:1.17-buster AS base_container
WORKDIR /data
ENV GOPATH=/data/.go
ENV PATH="$GOPATH/bin:${PATH}"
ENV GOCACHE="$GOPATH/go-build"
RUN mkdir -p .go
############ end of: base_container ############

############ go-build ############
FROM base_container as go-build
CMD make build-docker
############ end of: go-build ############

############ go_lint ############
FROM golangci/golangci-lint:v1.44.0 as go_lint
WORKDIR /data
ENV GOLANGCI_LINT_CACHE=/data/.go_lint
RUN mkdir -p .go_lint
CMD golangci-lint run -v --timeout=5m0s
############ end of: go_lint ############

############ tests ############
FROM base_container as test-unit
CMD make test-unit

FROM base_container as test-integration
CMD make test-integration

FROM base_container as test-coverage
CMD go install github.com/jstemmer/go-junit-report && \
  make test-coverage
############ end of: generate_mocks ############

############ proto ############
FROM base_container as base_buf_image
# install buf
RUN BIN="/usr/local/bin" && \
VERSION="1.0.0-rc11" && \
BINARY_NAME="buf" && \
  curl -sSL \
    "https://github.com/bufbuild/buf/releases/download/v${VERSION}/${BINARY_NAME}-$(uname -s)-$(uname -m)" \
    -o "${BIN}/${BINARY_NAME}" && \
  chmod +x "${BIN}/${BINARY_NAME}"

FROM base_buf_image as proto_lint
CMD buf lint && buf breaking --against https://gitlab.com/aleksei.e.kozlov/users-api.git#branch=main

FROM base_buf_image as compile_protobuf_files
CMD go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway \
    google.golang.org/protobuf/cmd/protoc-gen-go \
    google.golang.org/grpc/cmd/protoc-gen-go-grpc \
    github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2 && \
  buf generate
############ end of: proto ############

############ generate_mocks ############
FROM base_container as generate_mocks
CMD go install github.com/vektra/mockery/v2 && \
  go generate -x -v ./...
############ end of: generate_mocks ############

############ run-binary ############
FROM base_container as run-binary
CMD make run-binary-docker
############ end of: run-binary ############
