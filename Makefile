BIN_DIR:=.bin
BIN_DIR_DOCKER:=.bin_docker

.PHONY: build
build:
	echo $(BIN_DIR)
	[ -d $(BIN_DIR) ] || mkdir $(BIN_DIR)
	go build -o $(BIN_DIR) ./cmd/users-api/

build-docker: BIN_DIR:=$(BIN_DIR_DOCKER)
.PHONY: build-docker
build-docker: build

.PHONY: go-run-local
go-run-local:
	go run cmd/users-api/main.go

run-binary-docker: export USERS_API_PATH_TO_CONFIG=.configs/values_docker.yaml
.PHONY: run-binary-docker
run-binary-docker: 
	$(BIN_DIR_DOCKER)/users-api

.PHONY: test-unit
test-unit:
	go test -race -count 1 ./...

.PHONY: test-integration
test-integration:
	go test  -tags integration -race -count 1 ./...

.PHONY: test-coverage
test-coverage:
	go test -tags "integration" -coverprofile=coverage.out.tmp -cover ./... | tee test.log
	cat coverage.out.tmp | grep -v ".pb." | grep -v "mocks" | grep -v "main" > coverage.out
	go tool cover -html=coverage.out -o coverage.html
	go-junit-report -set-exit-code < test.log > test.xml
	go tool cover -func=coverage.out
